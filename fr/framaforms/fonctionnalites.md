# Fonctionnalités

## Création rapide de formulaires

### Glissez-déposez, pour plus de facilité !

Pour créer votre formulaire, aucune compétence technique particulière n'est nécessaire : il vous suffit de glisser-déposer les champs souhaités.

![glisser/déposer les composants](images/fonctionnalites_forms_creation_rapide.png)

### Création simple

Lorsque vous vous connectez sur votre compte Framaforms, la page d'accueil vous permet d'accéder rapidement à la création d'un nouveau formulaire.

![création simple](images/fonctionnalites_fomrs_creation_simple.gif)

### Création à partir d'un modèle

Vous pouvez également utiliser un modèle existant en furetant dans la rubrique « Créer un formulaire depuis un modèle ». Les modèles sont proposés par les autres utilisateurs et sont d'une grande diversité : n'hésitez pas à les cloner et les enregistrer pour y accéder, et choisir en fonction de vos propres besoins !

<video controls width="100%" height="450px"><source src="images/fonctionnalites_forms_clone.mp4"></video>

## Cloner des formulaires existants

### Privé

Besoin de réutiliser un formulaire que vous aviez déjà fait ? Ou de gagner du temps en utilisant ou adaptant un formulaire déjà prêt à l'usage ? Vous pouvez réutiliser un formulaire en le clonant : la structure sera dupliquée automatiquement, sans qu'aucune réponse ne soit copiée.

Pour ce faire vous devez&nbsp;:

  1. cliquer sur l'onglet **Partager**
  * cliquer sur le lien dans la section **Clonage** (`Pour cloner ce formulaire, suivez simplement ce lien :`)

Cela vous mènera à un nouveau formulaire qui sera déjà prérempli avec les champs du formulaire actuel, mais sans aucune réponse. Vous pourrez donc l'adapter à vos nouveaux besoins, sans que le formulaire courant ou ses résultats ne soient affectés.

### Publiquement
Partager vos formulaires ? Faire gagner du temps à d'autres utilisateurs ? Ou juste garder votre travail sous le coude pour la prochaine fois ? Il est possible d'enregistrer votre formulaire en tant que modèle.

Voici comment faire :

* Cliquez sur l'onglet **Modifier** de votre formulaire Framaforms
* Allez jusqu'à la section « **Autres options** » de la page
* Cochez-y la case « **Lister parmi les modèles** »
* Et ... Voilà !

![modèle de formulaire](images/fonctionnalites_forms_creation_modele.gif)

NB: seule la structure sera copiée (titre, description, image éventuelle, questions et champs à remplir), **pas les réponses**.

## Formulaire sur plusieurs pages

Votre formulaire est long… Trop long ? Vous pouvez le découper sur plusieurs pages afin d'en faciliter le remplissage, en glissant-déposant le champ "saut de page" aux endroits souhaités.

![multipages](images/fonctionnalites_forms_multipages.png)

## Multiple choix de champs

Que vous ayez besoin de champs textes, de cases à cocher, d'une liste de sélection, de donner la possibilité à vos utilisateurs de mettre en ligne des fichiers, etc... Framaforms vous propose de nombreux choix de champs qui vous permettront de créer un formulaire adapté à vos besoins.

![multiple choix de champs](images/fonctionnalites_forms_champs.png)

## Participation facile

Les framaforms s'adaptent automatiquement à votre support et peuvent être visualisés et remplis sur mobiles et tablettes.

![mobile](images/fonctionnalites_forms_mobile.png)

## Diffusion et intégration simplifiées

Vous pouvez facilement partagez vos fomulaires par email, sur un site web, ou sur les réseaux sociaux. Vous pouvez même les embarquer sur votre site web.

Vous trouverez l'onglet "Partager" en haut de votre formulaire.

**Plusieurs possibilités s'offrent à vous** :

* en copiant/collant l'adresse URL (dans un email ou sur votre site Web, par exemple),vous permettez aux personnes ciblées d'accéder au formulaire en un clic! Vous pouvez de la même manière le partager sur les réseaux sociaux.
* Framaforms vous fourni directement le code pour embarquer le formulaire, c'est-à-dire l'insérer dans un autre site Web (iframe). Pour ce faire, vous devez&nbsp;:
  * cliquer sur l'onglet **Partager** de votre formulaire
  * copier le code dans la section **Embarquer le code**
  * coller ce code dans votre site
    <p class="alert-warning alert">A noter que tous les sites ne permettent pas d'inclure des iframes dans leurs contenus. Il vous faudra peut-être disposer d'autorisations ou de plugins spécifiques (ex pour Wordpress).</p>


## Multiples thèmes

Par défaut, le thème est plutôt neutre, mais vous pourrez bientôt en choisir d'autres (en cours), plus adaptés ou adaptables - dans les grandes lignes - à votre charte graphique.

![Theme responsive standard](images/fonctionnalites_forms_theme1.png)

![theme Frama](images/fonctionnalites_forms_theme2.png)

Nous avons encore un peu de travail à fournir pour vous proposer un plus grand choix de thèmes. Mais nous ne vous avons pas oublié !

## Options avancées

### Champs conditionnels

Vous pouvez masquer/afficher/forcer un champ suivant les réponses de vos utilisateurs. Par exemple, si une case à cocher "Autre" est sélectionnée, vous pouvez faire apparaître un champ texte "autre" dans laquelle l'utilisateur devra préciser sa réponse.

![champs conditonnel 1](images/fonctionnalites_forms_conditionnel1.png)
![champs conditionnel 2](images/fonctionnalites_forms_conditionnel2.png)

[Voir comment faire](astuces.html#je-souhaite-envoyer-des-mails-en-fonction-des-réponses)

### Règles de validation

Vous pouvez ajouter à votre formulaire, si vous le souhaitez, des règles de validation qui pourront être simples ou complexes. Par exemple : « Vérifier que le champ "âge" soit compris entre 18 et 118 », ou « vérifier que le champ "kilomètres" soit supérieur à 12 ».

<video controls width="100%" height="450px"><source src="images/fonctionnalites_forms_validation.webm"></video>

### Courriels

Vous pouvez envoyer un email à chaque soumission, à vous même, au répondant, aux deux, ou même à qui vous voulez. Cet email peut, si vous le souhaitez, contenir les valeurs saisies dans le formulaire. Pour ce faire vous devez vous rendre dans **Formulaire** > **Courriels**.

![Courriel](images/fonctionnalites_forms_courriel.png)

Le mail par défaut comportera :

* la date de soumission : ``[submission:date:long]``
* le nom de l'utilisateur⋅trice (si cette personne est connectée à un compte framaforms) : ``[submission:user]``
* les valeurs soumises : ``[submission:values]``
* le lien vers les résultats : ``[submission:url]``

![modèle de Courriel](images/fonctionnalites_forms_courriel_modele.png)

Vous avez trois possibilités d'envoi :

* **Standard emails (always send)** : cette fonctionnalité permet d'envoyer un mail après chaque validation de formulaire
* **Confirmation request emails (always send)** : permet d'envoyer un mail à la soumission, avec un lien de validation sur lequel il faut cliquer pour valider la soumission du formulaire
* **Confirmation emails (only send when the confirmation URL is used)** : permet d'envoyer un mail **lorsque le/la répondant⋅e a cliquer sur le lien du mail de confirmation** (configuré ci-dessus)

### Validation de la soumission par email

Vous pouvez demander à ce que la soumission soit confirmée par email. Par exemple si vous voulez éviter qu'une même personne réponde plusieurs fois. Cette dernière recevra alors un email de confirmation. Tant que cet email n'aura pas été saisi, sa réponse au formulaire sera considérée comme invalide.

Pour ce faire vous devez utiliser **Confirmation request emails (always send)** dans **Formulaire** > **Courriels** et ajouter le jeton `[submission:confirm_url]` dans le modèle de courriel.

Par exemple&nbsp;:

```
Vous avez répondu au formulaire [current-page:title] le [submission:date:long]
Les valeurs soumises sont:
[submission:values]

Merci de bien vouloir confirmer votre participation en cliquant sur : [submission:confirm_url]
```

Vous pouvez ajouter un email à envoyer après confirmation depuis **Confirmation emails (only send when the confirmation URL is used)**.

### Page de confirmation personnalisée

Vous pouvez définir un message de confirmation, ou renvoyer le répondant sur la page de votre choix (votre site web, par exemple)

![confirmation](images/fonctionnalites_forms_confirmation.png)

Pour cela, allez dans **Paramètres avancés du formulaire**, puis :

1) entrez le message que toute personne ayant répondu au formulaire verra

2) (optionnel) cliquez sur **Explorer les jetons disponibles** pour insérer des messages spéciaux

3) (optionnel) choisir une variable (ici, une date)

4) (optionnel) cette variable affichera la date au format long : ``Vendredi, août 18, 2017 - 14:34``

5) après avoir répondu au questionnaire, la réponse personnalisée s'affiche

![confirmation gif](images/fonctionnalites_forms_confirmation.gif)

Vous pouvez aussi rediriger les répondant⋅e⋅s vers une adresse particulière en mettant, par exemple, ``https://chatons.org`` pour **Url personnalisée**

### Limitation du nombre de soumissions

Vous pouvez limiter le nombre total de soumissions à un chiffre inférieur à 1000 (mais pas au-dessus, voir la page [limitations](https://framaforms.org/limitations))

Vous pouvez aussi limiter le nombre de soumissions pour un même utilisateur (identifié par un cookie anonymisé)

![limitations](images/fonctionnalites_forms_limitations.png)

### Désactivation/fermeture du formulaire.

Par défaut, votre formulaire est actif pendant 6 mois (9 mois maximum, voir la page [limitations](https://framaforms.org/limitations)), mais vous pouvez le fermer à la soumission quand vous le souhaitez, tout en conservant la possibilité d'y accéder.

Pour fermer un formulaire vous devez&nbsp;:

  1. aller dans **Formulaire**
  * cliquer sur **Paramètres avancés du formulaire**
  * dans la section **Paramètres de soumission** aller jusque **Statut de ce formulaire**
  * cocher **Fermé**
  * cliquer sur le bouton **Enregistrer la configuration**

![fermeture formulaire](images/fonctionnalites_forms_fermeture.png)

## Analyse et téléchargement

### Visualisation

Vous pouvez facilement accéder à la liste des soumissions, et visualiser ces dernières les unes à la suite des autres.

![Visualisation](images/fonctionnalites_forms_visualisation.png)

### Téléchargement

Les soumissions peuvent être téléchargées aux formats tableurs habituels (``.csv`` pour Libre Office Calc ou Excel) en vue de traitements plus complexes.

![téléchargement](images/fonctionnalites_forms_telechargement.png)

Pour récupérer vos résultats :

* cliquez sur **Résultats** puis **Téléchargement**
* (optionnel) vous pouvez sélectionner des options, mais la sélection par défaut couvre la plupart des besoin
* cliquez sur **Téléchargement** (tout en bas)
* ouvrez le fichier avec LibreOffice Calc (ou excel) ou enregistrez-le sur votre ordinateur

![résultats dans LibreOffice Calc](images/fonctionnalites_forms_telechargement_tsv.png)

### Analyse

Un outil d'analyse des réponses (très basique) est inclus, vous permettant d'obtenir immédiatement des statistiques. Par exemple, le nombre de réponses sur un champ texte, le total ou la moyenne sur les champs numériques, ou le détail des réponses sur les champs à choix multiples.

![analyse](images/fonctionnalites_forms_analyse.png)

### Graphes

Les réponses analysées peuvent faire l'objet d'un graphique automatiquement généré. (NB : pour des graphiques plus complets, téléchargez vos résultats dans un tableur comme Libre Office)

![graphes](images/fonctionnalites_forms_graphes.png)

### Partage des résultats

#### Avec un compte Framaforms

Vous pouvez partager les résultats (**pas l'administration**) avec un compte Framforms. Pour cela vous devez&nbsp;:

  1. vous rendre dans l'onglet **Modifier**
  * puis **Autres options**
  * puis **Utilisateurs ayant accès aux résultats**
  * entrer le nom du compte avec lequel vous souhaitez partager les résultats
  * cliquer sur **Enregistrer** tout en bas

Il vous reste à donner le lien de votre formulaire au compte&nbsp;: celui-ci verra alors l'onglet **Résultats** sur cette page.

#### Résultats publics

Il est possible de partager les résultats publiquement (sans nécessité d'avoir un compte pour les voir).

<p class="alert alert-warning">ATTENTION : En choisissant "Oui", l'ensemble des résultats de votre formulaire sera accessible <b>à tout le monde</b>. Même si les personnes ne peuvent pas modifier les résultats, cela n'est <b>PAS RECOMMANDÉ</b> car vous pouvez rendre public des informations personnelles (type : nom, prénom, email, adresses ou n° de téléphone) de personnes qui ne souhaitaient pas les rendre publiques.</p>

Pour cela vous devez&nbsp;:
  1. vous rendre dans l'onglet **Modifier**
  * puis **Autres options**
  * puis **Accès public aux résultats**
  * sélectionner `Oui`
  * cliquer sur **Enregistrer** tout en bas

Il vous reste à donner le lien de votre formulaire&nbsp;: sera alors affiché l'onglet **Résultats publics** sur cette page.

## Respect de vos utilisateurs et de vos données

### Vie privée garantie

Framaforms est un service proposé par Framasoft, association loi de 1901 d'intérêt général dont un des objectifs est de résister aux atteintes à la vie privée, notamment lorsqu'il s'agit de collectes massives telles que pratiquées par GAFAM (Google, Apple, Facebook, Amazon, Microsoft). Nous prenons l'engagement de ne pas exploiter vos données ou celles de vos utilisateurs ([voir CGU](https://framasoft.org/nav/html/cgu.html)).

### Anonymat

Les données issues de Framaforms sont anonymisées, il est (normalement) impossible pour un créateur de formulaire de récupérer les adresses IP des répondants. Il va de soi cependant que si le créateur du formulaire place des champs « Nom » ou « Prénoms » et que le répondant les remplit avec des données correctes, ces données pourront être récupérées (c'est pourquoi il ne faut **JAMAIS** saisir un mot de passe dans un framaform !).

### Logiciels libres

Framaforms repose entièrement sur des logiciels libres, qui autorisent et permettent [une installation du services sur vos propres serveurs](https://framacloud.org/fr/cultiver-son-jardin/framaforms.html).
