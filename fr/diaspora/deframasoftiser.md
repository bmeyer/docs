# Déframasoftiser Framasphère

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Exporter

Pour exporter vos données, vous devez&nbsp;:

  * cliquer sur votre avatar dans le coin supérieur droit
  * cliquer sur **[Paramètres](https://framasphere.org/user/edit)**
  * cliquer sur le bouton **Demander mes données de profil** dans la section **Exporter des données**
  * attendre l'export «&nbsp;Nous sommes en train de traiter vos données. Veuillez vérifier à nouveau l'avancement dans un moment.&nbsp;»
  * cliquer sur le bouton **Télécharger mon profil** après avoir rechargé la page
  * télécharger le fichier `.json.gz` sur votre ordinateur
