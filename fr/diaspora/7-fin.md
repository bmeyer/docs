7ème partie - Dernière ligne droite
-----------------------------------

Nous avons maintenant couvert la plupart des fonctionnalités principales
de diaspora\*. La dernière partie de ce tutoriel traite des quelques
bricoles que vous devez encore savoir.

## Connecter votre graine diaspora\* à d'autres services.

Vous pouvez connecter votre graine diaspora\* à différents autres
services : Facebook, Twitter et Tumblr. Bien sûr, pour cela il faut que
vous ayez un compte sur ces services !

Avant tout, assurez-vous d'avoir vos informations de connexion pour ces
services à portée de main car vous en aurez besoin pour autoriser la
connexion.

Pour vous connecter à un autre service , cliquez sur son icône dans la
zone Services connectés dans la colonne de droite du flux. Si votre
compte est déjà authentifié sur ce service dans une autre fenêtre du
même navigateur, il devrait vous reconnaître automatiquement. Dans le
cas contraire, il vous demandera de le faire maintenant. Une fois
authentifié, il vous demandera si vous autorisez la publication sur ce
service. Dès que vous acceptez, vous êtes prêt à publier sur ce service
directement depuis votre graine diaspora\* !

La prochaine fois que vous cliquerez dans l'éditeur, vous verrez l'icône
de ce service sous l'éditeur. Cliquez sur cette icône pour l'activer et
votre message apparaîtra également dans le flux de cet autre service.
Sympa non ? Pour plus d'informations à propos de la publication de
messages sur d'autres services, voyez la [Partie
5](5-partage.html).

## Gestion du compte

La dernière chose que nous devons voir pour vous aider à démarrer est la
page des paramètres de l'utilisateur. Pour l'atteindre, cliquez sur
votre photo ou votre nom dans la barre d'en-tête et, depuis le menu
déroulant, cliquez sur Paramètres. Vous obtenez une vue d'ensemble des
options de votre compte.

Il y a cinq onglets dans la page des paramètres du compte : **Profil**, **Compte**, **Vie privée**, **Services** et **Applications**.

### Profil

Sur cette page vous pouvez changer, ajouter ou supprimer n'importe quelle information sur votre profil. Retournez à la fin de la [partie
1](1-connexion.html) si vous souhaitez vous rappeler comment faire. N'oubliez pas d'appuyer sur le bouton « Mettre à jour le profil » une fois fini !

### “NSFW”

diaspora\* utilise un standard auto-géré appelé NSFW (*not safe for work*). Si vous êtes susceptible de publier beaucoup de contenu qui pourrait ne pas être adapté [à notre charte de modération](https://framasoft.org/fr/moderation) (voir [notre section Modération](../moderation/moderation_diaspora.html)), merci de cliquer la case NSFW. Cela cachera vos posts dans le flux des autres membres derrière un bandeau marqué NSFW. Ils/elles pourront alors cliquer pour voir le post&nbsp;:

![post diaspora caché par un bandeau nsfw](images/diaspora-nsfw.png)

En laissant la case vide, vous vous engagez à ne pas poster de contenu qui pourrait porter préjudice à quelqu'un le regardant au travail. Si, par contre, vous souhaitez poster ce genre de contenu occasionnellement, vous pouvez laisser la case vide et ajouter le tag ``#nsfw`` dans le post, ce qui aura pour effet de le cacher par un bandeau.

Pour activer ou désactiver cette fonctionnalité, vous devez&nbsp;:

  1. aller dans **Paramètres**
  * aller dans [Profil](https://framasphere.org/profile/edit)
  * descendre jusqu'à la section **NSFW**
  * cocher ou décocher **Marquer tout ce que je poste comme #NSFW**
  * cliquer sur le bouton **Mettre à jour le profil**

### Compte

Ici, vous pouvez changer votre adresse email et votre mot de passe,
définir votre langue, choisir vos préférences de notification par email
et télécharger vos données ou fermer votre graine. Notre objectif est
que vous puissiez utiliser les données téléchargées pour migrer votre
graine vers un autre pod, bien que cela ne soit pas encore possible.
Vous pouvez toutefois télécharger vos données périodiquement à titre de
sauvegarde.

diaspora\* dispose d'un standard auto-régulé par la communauté nommé
NSFW (Not Safe For Work - Pas Sûr Au Travail). Si vous êtes susceptible
de publier du matériel qui pourrait ne pas convenir à quiconque le
regarde au bureau (peut-être parce-que leur patron se tient derrière
eux), veuillez envisager de cocher la case NSFW. Cela masquera vos
publications dans le flux des autres personnes derrière un bouton qui
préviendra que le message est marqué comme NSFW. Ils pourront cliquer
sur le bouton pour faire apparaître le message s'ils le souhaitent.

### Actualités de la communauté

Dans la page ds paramètres, vous pouvez aussi activer « les actualités de la communauté » si la fonctionnalité est disponible sur votre *pod*. Les « actualités de la communautés » affichent les posts de personnes de la communauté que votre *podmin* jugent intéressantes. Cela peut être un bon moyen de trouver des personnes avec qui partager en arrivant sur diaspora\*.

### Vie privée

Ceci est la liste des personnes que vous ignorez. Vous pouvez les
supprimer de cette liste si vous souhaitez à nouveau voir leurs
publications. Voyez la [Partie
5](5-partage.html) pour en
savoir plus sur le fait d'ignorer des gens.

### Double authentification (TOTP)

La double authentification est un moyen efficace de garantir que vous serez le seul à pouvoir vous identifier avec votre compte. À la connexion, vous saisirez un code à 6 chiffres en plus de votre mot de passe afin de prouver votre identité.

<p class="alert-warning alert">Soyez prudent néanmoins : si vous égarez votre téléphone et vos codes de récupération créés à l'activation de cette fonctionnalité, <b>vous perdrez définitivement l'accès à votre compte diaspora*</b>.</p>

Pour activer la double authentification vous devez&nbsp;:
  1. vous rendre [dans vos paramètres > Double authentification](https://framasphere.org/two_factor_authentication)
  * cliquer sur le bouton **Activer**
  * scanner le code QR avec une application comme FreeOTP (disponible sur l'[AppStore](https://itunes.apple.com/us/app/freeotp-authenticator/id872559395?mt=8), le [Play Store](https://play.google.com/store/apps/details?id=org.liberty.android.freeotpplus) et [F-Droid](https://f-droid.org/packages/org.liberty.android.freeotpplus/)) en appuyant sur <i class="fa fa-ellipsis-v" aria-hidden="true"></i> puis sur **Scan QR Code**
  * entrer le code obtenu par l’application en appuyant sur votre identifiant dans la liste, dans le champ **Jeton de double authentification**
  * cliquer sur le bouton **Confirmer et activer**

Vous obtenez alors les codes de récupération à **converser précieusement** car si vous les perdez vous ne pourrez plus accéder à votre compte&nbsp;: [nous ne désactiverons pas la double authentification](https://contact.framasoft.org/fr/faq/#2fa_desactivation).

Ces codes servent si vous perdez l'accès à votre téléphone. Vous pouvez alors utiliser l'un de ces codes de récupération pour récupérer l'accès à votre compte diaspora\*. **Gardez ces codes dans un lieu sûr**. Par exemple, vous pouvez les imprimer et les ranger avec vos autres documents importants.

Pour vous connecter à votre compte, vous devrez désormais, en plus de votre identifiant et de votre mot de passe, utiliser un code généré par l'application TOTP.

Pour désactiver cette double authentification vous devez retourner dans [vos paramètres](https://framasphere.org/two_factor_authentication) et entrer votre mot de passe dans la section **Désactiver**.

### Services

La page des services indique les services auxquels vous êtes connecté(e)
(ex: Facebook, Twitter, Tumblr) et vous permet de connecter de nouveaux
services à votre graine diaspora\*.

### Applications

Cet onglet affiche la liste des applications que vous avez autorisé à se connecter à votre compte diaspora\*.
 

Merci infiniment d'avoir lu ce guide de démarrage ! Nous espérons qu'il
vous a été utile et que vous vous sentez maintenant suffisamment à
l'aise pour utiliser diaspora\* comme votre nouvelle "maison sur
internet". Si vous avez la moindre question, n'hésitez pas à envoyer un
message public sur diaspora en incluant les tags \#question et \#diaspora.

[Sixième partie - Notifications et
conversations](6-notification-conversation.html)
| [Tutoriels diaspora\*](https://diasporafoundation.org/tutorials)

### Ressources utiles

-   [Base de code](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Trouver et signaler des
    bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - Général](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Développement](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Liste - Général](http://groups.google.com/group/diaspora-discuss)
-   [Liste - Développement](http://groups.google.com/group/diaspora-dev)

[![Licence Creative
Commons](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) est protégé
sous licence [Licence publique Creative Commons Attribution 3.0 non
transposée](http://creativecommons.org/licenses/by/3.0/)
