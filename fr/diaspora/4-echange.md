4ème partie - Trouver des gens et se connecter avec eux
-------------------------------------------------------

Il est maintenant temps d'ajouter des contacts à l'aspect "Diaspora" que
vous venez de créer. Si vous connaissez déjà des gens qui utilisent
diaspora\*, vous pouvez les trouver et les ajouter à l'aspect que vous
voulez.

## Partage

On appelle le fait de se connecter à quelqu'un "partager" car c'est
l'indication que vous souhaitez échanger avec lui/elle. Le partage dans
diaspora\* est une notion qui peut être déroutante au début car le degré
de partage que vous pourrez avoir avec les gens ne sera probablement pas
le même que leur degré de partage avec vous. Essayons de donner un sens
à tout cela.

Sur diaspora\*, il y a trois sorte de relations entre les utilisateurs:

### Abonnés

Quelqu'un vous a placé dans l'un de ses aspects mais vous n'en avez pas
fait autant. Vous serez informé que cette personne a "commencé à
partager avec vous" mais vous ne remarquerez rien d'autre.

Ils verront vos messages publics dans leurs Flux mais aucun des messages
que vous destinez à un public restreint.

Vous n'avez aucun moyen de savoir dans quel(s) aspect(s) quelqu'un vous
a placé, pour des raisons de confidentialité - c'est son affaire de
décider dans quel(s) aspect(s) il/elle place ses contacts donc personne
d'autre ne peux le savoir.

### Suivant

Vous partagez avec quelqu'un qui ne partage pas avec vous. Cela veut
dire que vous l'avez ajouté(e) à un (ou plusieurs) de vos aspects mais
qu'il/elle ne vous a pas ajouté dans les siens.

La personne que vous suivez aura accès aux messages que vous publiez
dans les aspects dans lesquels vous l'avez placée ainsi que dans vos
messages publics et vous verrez ses messages publics dans votre flux.

### Partage mutuel

Si vous partagez mutuellement avec une personne, les choses deviennent
plus intéressantes et plus compliquées ! Vous pouvez comparer ça à être
"amis" sur Facebook bien qu'il y ait des différences importantes.

Quand la connexion est mutuelle, vous avez tous les deux exprimé le
souhait de voir vos publications respectives donc chacun d'entre vous
verra les publications placées dans les aspects dans lesquels il/elle
aura été ajouté(e). Toutefois, votre degré de partage peut être très
différent. Imaginez le scénario suivant :

-   Vous considérez que Jill est une vague connaissance et la placez
    dans votre aspect "Connaissances".
-   En revanche, Jill pense que vous êtes l'un(e) de ses meilleur(e)s
    ami(e)s et vous ajoute à son aspect "Amis proches".
-   Vous publiez essentiellement dans vos aspects "Amis" et "Famille"
    donc Jill ne voit pas beaucoup de vos messages.
-   D'un autre côté, du fait que vous vous trouvez dans l'aspect "Amis
    proches" de Jill, vous voyez la plupart de ses messages.

La chose principale à garder en mémoire est que personne ne verra un de
vos messages à moins que l'ayez choisi public ou publié dans un aspect
dans lequel vous avez manuellement ajouté cette personne.

## Trouver des gens

Pour pouvoir commencer à partager avec des gens, vous allez devoir
commencer à suivre des gens. Peut-être qu'ils vous suivront en retour !
Voyons maintenant comment faire cela.

Il y a plusieurs manières de trouver quelqu'un et de l'ajouter à un
aspect.

### Recherche

Le champ de recherche se trouve dans la barre d'en-tête. Pour ajouter
quelqu'un en le cherchant :

-   Tapez un nom ou un ID diaspora\*. Des suggestions vont apparaître
    dès que vous commencerez à taper.
-   Appuyez sur entrée pour démarrer la recherche et vous aboutirez à
    une page de résultats.
-   Quand vous avez trouvé la personne que vous cherchiez, cliquez sur
    le bouton Ajouter contact.
-   Si la personne avec laquelle vous souhaitez commencer à partager
    apparaît dans la liste des suggestions, sélectionnez simplement son
    nom pour accéder à sa page de profil et cliquez sur le bouton
    Ajouter contact que vous y trouverez.
-   Un menu déroulant apparaîtra alors sous le bouton dans lequel vous
    pourrez choisir dans quel aspect ajouter cette personne. Vous pouvez
    également créer un nouvel aspect et l'ajouter à cet aspect en
    cliquant sur *Ajouter un aspect*.

![Add
contact](images/echange-1.png)

### Envoyer un courriel d'invitation

Une autre façon de suivre un(e) ami(e) est de lui envoyer une invitation
par email. Vous pouvez le faire depuis le lien d'invitation dans le menu gauche dans [votre flux](https://framasphere.org/stream) :

![Invite](images/echange-2.png)

Ou dans le menu latéral droit si vous êtes sur la page de recherche de contacts :

![Invite via la recherche](images/echange-2-1.png)

Dès que votre ami(e) acceptera l'invitation, il/elle sera amené(e) à passer par les mêmes étapes d'inscription que celles par lesquelles vous venez de passer.

### Depuis leur profil

Une autre manière d'ajouter une personne à l'un de vos aspects est de
cliquer sur son nom, où que vous le voyiez sur diaspora\*. Cela vous
amènera sur sa page de profil et vous pourrez, de là, l'ajouter à vos
aspects depuis le bouton situé dans le coin en haut à droite de la page.

Vous pouvez encore simplement passer votre souris au-dessus de son nom
ou de sa photo de profil dans le flux et une petite "carte flottante"
apparaîtra. Vous pourrez l'ajouter à un aspect directement depuis cette
carte flottante.

### Suivre des \#tags

Les trois options ci-dessus concernent le fait de se connecter à des
personnes que vous connaissez déjà. Mais parfois les étrangers peuvent
être tout aussi intéressants, voire encore plus. Une bonne façon de
commencer à se connecter avec des gens consiste à suivre des \#tags de
sujets qui vous intéressent. Les publications contenant ces tags
apparaîtront dans votre flux et vous pourrez suivre les personnes que
vous trouvez intéressantes en les plaçant dans l'un de vos aspects.

## Supprimer quelqu'un de vos contacts

Pour retirer quelqu'un de vos contacts complètement et donc arrêter de
partager avec lui/elle, tout ce que vous avez à faire c'est le/la
retirer des aspects où vous l'avez placé(e). Vous pouvez :

1.  aller sur sa page de profil ou passer votre souris sur son nom dans
    le flux et attendre l'apparition de la carte flottante.
2.  cliquer sur la flèche sur le bouton vert de sélection des aspects et
    dé-sélectionner tous les aspects cochés.

C'est tout. Une fois qu'il/elle n'est plus dans vos aspects, vous ne
partagez plus rien avec cette personne.

Vous pouvez également retirer une personne de vos aspects via votre page
de contacts, ce que nous avons vu à la fin de la [Partie
3](3-aspects.html).

Vous savez maintenant comment commencer à partager avec des gens et
espérons que vous avez pu ajouter quelques contacts. Vous allez
maintenant commencer à voir du contenu de leur part dans votre flux ; et
une fois que des gens auront commencé à vous suivre, vous aurez une
audience pour vos publications. **Il est temps de commencer à publier du
contenu.**

[3ème partie - Les
aspects](3-aspects.html) |
[Cinquième partie - Commencez à partager
!](5-partage.html)

### Ressources utiles

-   [Base de code](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Trouver et signaler des
    bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - Général](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Développement](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Liste - Général](http://groups.google.com/group/diaspora-discuss)
-   [Liste - Développement](http://groups.google.com/group/diaspora-dev)

[![Licence Creative
Commons](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) est protégé
sous licence [Licence publique Creative Commons Attribution 3.0 non
transposée](http://creativecommons.org/licenses/by/3.0/)
