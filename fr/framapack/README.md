# Framapack

Pratique, sur Framapack on fait son marché des meilleurs logiciels libres Windows dans leur dernière version.
On les sélectionne et ils s’installent tous ensemble d’un simple clic !

Pour vous expliquer le principe, voici une présentation de Pyves réalisée avec le logiciel RevealJS et les dessins de [Gee](http://ptilouk.net/) sous licence CC-By-SA.

<div class="embed-responsive embed-responsive-16by9">
  <iframe src="https://framapack.org/cquoi/" class="embed-responsive-item" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
</div>