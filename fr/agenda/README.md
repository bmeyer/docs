# Documentation pour <b class="frama">Fram</b><b class="vert">agenda</b>

[<b class="frama">Frama</b><b class="vert">drive</b>](https://framadrive.org) et [<b class="frama">Fram</b><b class="vert">agenda</b>](https://framagenda.org) sont des services libres proposé par [<b class="frama">Frama</b><b class="soft">soft</b>](https://framasoft.org) dans le cadre de sa campagne [**Dégooglisons Internet**](https://degooglisons-internet.org/). <b class="frama">Frama</b><b class="vert">drive</b> est un service **d’hébergement de fichiers** alors que <b class="frama">Fram</b><b class="vert">agenda</b> est une application de gestion d’**agendas**, de **contacts** et de **tâches**.

Ces services sont tout deux basés sur le logiciel libre [NextCloud](https://nextcloud.com/) et sur plusieurs de ses applications.

## Framagenda

![capture d’écran Framagenda](images/framagenda-10.png)

Découvrir un [exemple d’utilisation de Framagenda](exemple-d-utilisation.md).

[Framagenda](https://framagenda.org) vous permet&nbsp;:

-   La création d’un compte (sur une instance
    [Nextcloud](https://nextcloud.com/), mais avec 15 Mo d’espace disque,
    ce n’est pas un [Framadrive](https://framadrive.org))&nbsp;;
-   La création et l’édition de multiples agendas (perso, pro,
    associatif, fêtes familiales, etc.)&nbsp;;
-   La création d’événements (rendez-vous) dans un agenda&nbsp;:
    -   Privé, confidentiel, public…&nbsp;;
    -   Possibilité de détailler&nbsp;: horaires, lieux, description…&nbsp;;
    -   Possibilité de faire des rappels&nbsp;;
    -   Récurrence&nbsp;: possibilité de paramétrer des événements qui se
        répètent régulièrement&nbsp;;
    -   Possibilité d’ajouter des participant-e-s par email (avec envoi
        d’email & d’un fichier .ics en pièce jointe)&nbsp;;
-   L’intégration avec un carnet de contacts (le calendrier de leurs
    anniversaires est automatiquement créé \\o/)&nbsp;;
-   L’intégration avec les listes de tâches (une par agenda, mais plus
    si affinités)&nbsp;;
-   La synchronisation avec vos appareils (exemple pour Android&nbsp;: via
    [DAVx⁵](https://www.davx5.com/))
    -   de vos agendas (avec un choix agenda par agenda)&nbsp;;
    -   des listes de tâches afférentes (exemple pour Android&nbsp;: avec
        [OpenTasks](https://play.google.com/store/apps/details?id=org.dmfs.tasks&hl=fr)) ou [Tasks](https://f-droid.org/fr/packages/org.dmfs.tasks/)&nbsp;;
    -   de vos contacts (toujours via
        [DAVx⁵](https://www.davx5.com/) pour Android)&nbsp;;
-   Le partage d’un ou plusieurs agendas avec d’autres utilisateurs de
    Framagenda (par leur pseudo)&nbsp;;
-   L’abonnement à d’autres agendas/calendriers externes (intégration
    via ics/WebCal, dont les calendriers des GAFAM&nbsp;: Gmail, Apple,
    Outlook, etc.)&nbsp;;
-   La création de liens publics vers chacun de vos agendas&nbsp;:
    -   Lien «&nbsp;vue publique&nbsp;», toute simple&nbsp;;
    -   Lien CalDAV pour les clients (Thunderbird, DAVx⁵, etc.)&nbsp;;
    -   Lien WebDAV pour ajouter dans Google Agenda & Cie&nbsp;;
-   La possibilité de publier un agenda sur votre site web (code
    d’intégration
    [iframe](https://fr.wikipedia.org/wiki/%C3%89l%C3%A9ment_HTML#Jeu_de_cadres))&nbsp;;
-   L’import [ics](https://fr.wikipedia.org/wiki/ICalendar) (dans un
    nouvel agenda ou dans un agenda existant)&nbsp;;
-   L’export [ics](https://fr.wikipedia.org/wiki/ICalendar) (agenda
    ou événement)&nbsp;;

### Interface

#### [Inscription & Connexion](Inscription-Connexion.md)
* S’inscrire sur Framagenda
* Se connecter à Framagenda
* Utiliser l’authentification en deux étapes (TOTP)
* Utiliser des mots de passe d’application

#### [Agenda](Interface-Agenda.md)
* Créer et modifier un calendrier
* Créer et modifier un événement
* Partager un événement avec d’autres utilisateurs de Framagenda
* Partager un événement par lien public
* Ajouter un abonnement

#### [Contacts](Interface-Contacts.md)
* Créer et modifier un contact
* Gérer ses carnets d’adresses
* Importer et exporter des contacts
* Partage des contacts

#### [Tâches](Interface-Tasks.md)
* Créer, modifier et valider une tâche.
* Créer des sous-tâches

### Synchronisation avec un client

#### Bureau
* [Thunderbird](Synchronisation/Thunderbird.md) (PC, MacOS, GNU/Linux, \*BSD)
* [Apple Calendar](Synchronisation/ical.md) (MacOS X)
* [Apple Contacts](Synchronisation/icard.md) (MacOS X)
* [Gnome](Synchronisation/gnome.md) avec Evolution, Agenda et Contacts (GNU/Linux, \*BSD)
* [KOrganizer](Synchronisation/kde.md) (GNU/Linux)

#### Mobile
* [DAVx⁵](Synchronisation/Android.md) (Android)
* [iOS](Synchronisation/iOS.md) (paramètres internes)
* [Windows Phone](Synchronisation/WindowsPhone.md) (paramètres internes)
* [Ubuntu Touch](Synchronisation/UbuntuTouch.md) (paramètres internes)
* [Jolla/SailFish OS](Synchronisation/Jolla.md) (paramètres internes)

### [FAQ](FAQ.md)
Votre question se trouve peut-être ici&nbsp;!

### Présentation vidéo

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://framatube.org/videos/embed/d6f5c3ee-0180-4390-8e8c-a061c270d99f" frameborder="0" allowfullscreen></iframe>

### Pour aller plus loin&nbsp;:

-   Essayer [Framagenda](https://framagenda.org)
-   Le [site officiel](https://nextcloud.com/) de Nexcloud (à
    soutenir&nbsp;!)
-   [Participer au code](https://github.com/nextcloud) de Nextcloud
-   Installer [Nextcloud sur vos
    serveurs](http://framacloud.org/cultiver-son-jardin/installation-de-nextcloud/)
-   [Dégooglisons Internet](https://degooglisons-internet.org/)
-   [Soutenir Framasoft](https://soutenir.framasoft.org/)
