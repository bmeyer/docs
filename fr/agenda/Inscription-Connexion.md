## S'inscrire sur Framagenda
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

L'inscription à Framagenda nécessite une adresse email.

Pour commencer, cliquez sur le lien **S'enregistrer** :

![framagenda-register-01](images/framagenda-register-01.png)

On vous demande de rentrer une adresse de courriel qui devra être confirmée par la réception d'un email :

![framagenda-register-02](images/framagenda-register-02.png)

Une nouvelle page s'affiche pour vous confirmer l'envoi du courriel de vérification :

![framagenda-register-03](images/framagenda-register-03.png)

Vous devez recevoir un email comme celui-ci. Si vous ne recevez rien à l'adresse renseignée, pensez à vérifier le dossier des spams ou bien à vérifier que vous avez entré votre adresse email correctement.

![register-3](images/register-3.png)

En cliquant sur le lien contenu dans l'email, vous pouvez créer votre compte en choisissant un nom d'utilisateur et un mot de passe.

![framagenda-register-04](images/framagenda-register-04.png)

En validant, vous êtes inscrit et redirigé dans l'application.


## Se connecter à Framagenda

Pour vous connecter à Framagenda, remplissez les champs avec vos informations de connexion.

![Framagenda-connexion](images/Framagenda-connexion.png)

## [Facultatif] Utiliser l'authentification en deux étapes (2FA)

Pour une sécurité de votre compte renforcée, Framagenda propose l'authentification en deux étapes (<abbr title="Two Factor Authentification">2FA</abbr>), par le système <abbr title="Time-based One-time Password Algorithm">TOTP</abbr>. Si cette option est activée lorsque vous vous connectez depuis un nouveau navigateur ou que votre session a expiré, vous serez invité à vous reconnecter. Après avoir entré vos informations de connexion comme d'habitude, Framagenda vous demandera d'entrer un code généré par votre téléphone.

Il est impératif de générer et garder dans un lieu sûr les codes de récupération avant d'activer l'authentification en deux étapes. Sinon vous perdrez votre accès à Framagenda si vous perdez votre téléphone.

<p class="alert-info alert">
<b>Note</b> : Si vous utilisez l'authentification en deux étapes, vous devez utiliser les mots de passe d'application (voir ci-dessous).
</p>

### Codes de récupération si vous perdez votre second facteur

Si vous voulez utiliser l'authentification en deux étapes, vous devez **toujours** générer les codes de récupération pour l'authentification en deux étapes. Si votre appareil que vous utilisez comme second facteur est volé ou ne fonctionne plus, vous pourrez utiliser un de ces codes pour se connecter à votre compte. Ils fonctionnent comme solution de rechange au second facteur.

Pour générer les codes de récupération, cliquez sur votre nom d'utilisateur en haut à droite de l'interface, puis cliquer sur la ligne [Paramètres](https://framagenda.org/settings/user) :

![framagenda-2FA-01](images/framagenda-2FA-01.png)

Dans la nouvelle page qui s'affiche, cliquez sur la ligne [Sécurité](https://framagenda.org/settings/user/security) dans le menu latéral gauche :

![framagenda-2FA-02](images/framagenda-2FA-02.png)

À la section **Authentification en deux étapes**, cliquez sur le bouton **Générer des codes de récupération**.

![framagenda-2FA-03](images/framagenda-2FA-03.png)

Vous aurez alors une liste de codes à usage unique :

![framagenda-2FA-04](images/framagenda-2FA-04.png)

Vous devez garder ces codes dans un lieu sûr, où vous pourrez les trouver. Ne les mettez pas au même endroit que votre second facteur, comme votre téléphone, et faites en sorte que si vous perdez l'un, vous aurez l'autre. L'imprimer et le garder chez soi est probablement la meilleure chose à faire.

### Installation d'une application d'authentification en deux étapes

Il existe beaucoup d'applications permettant d'effectuer l'authentification en deux étapes. Nous allons prendre les cas de Google Authentificator et FreeOTP, cette dernière application étant libre et installable à partir de [F-Droid](Divers/FDroid.md).

Google Authentificator est disponible sur l'[AppStore](https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8) pour iOS et sur le [Play Store](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) pour Android, tandis que FreeOTP est disponible sur l'[AppStore](https://itunes.apple.com/us/app/freeotp-authenticator/id872559395?mt=8), le [Play Store](https://play.google.com/store/apps/details?id=org.liberty.android.freeotpplus) et [F-Droid](https://f-droid.org/packages/org.liberty.android.freeotpplus/).

### Activation de l'authentification en deux étapes dans Framagenda

Pour que votre compte utilise désormais l'authentification en deux étapes, cliquez sur votre nom d'utilisateur en haut à droite de l'interface, puis cliquer sur la ligne [Paramètres](https://framagenda.org/settings/user) :

![framagenda-2FA-01](images/framagenda-2FA-01.png)

Dans la nouvelle page qui s'affiche, cliquez sur la ligne **Sécurité** à gauche de l'interface :

![framagenda-2FA-02](images/framagenda-2FA-02.png)

À la section **Authentification en deux étapes**, descendez jusqu'à la section **TOTP (Authenticator app)**, puis cochez la case **Activer les mots de passe à usage unique (TOTP)**. Un QR Code apparaît alors :

![framagenda-TOTP-01](images/framagenda-TOTP-01.png)

Ouvrez alors votre application 2FA précédemment installée et configurez un nouveau compte. Dans le cas de Google Authentificator, cliquez sur le menu en haut à droite, puis **Configurer un nouveau compte**, choisissez ensuite **Scanner un code barre**. Dans le cas de FreeOTP, appuyez simplement sur le code QR présent dans la barre d'outils.
Vous pouvez alors scanner le QR code sur votre écran. Les applications sont alors configurées automatiquement.

### Connexion avec TOTP

Lorsque vous vous connectez, après avoir entré vos informations de connexion comme d'habitude, Framagenda vous demande un code :

![framagenda-TOTP-02](images/framagenda-TOTP-02.png)

Ouvrez votre application 2FA sur votre appareil Android ou iOS et recopiez dans le champ le code qui s'affiche dans l'application :

![Framagenda-TOTP-3](images/TOTP-3.png)

Après validation, vous êtes alors connecté à Framagenda.

## Utiliser les mots de passe d'application

Framagenda permet de créer des mots de passe d'application, c'est-à-dire des mots de passe **spécifiques pour les applications de synchronisation**. L'utilisation des mots de passe d'application est nécessaire lorsque l'authentification en deux étapes est activée, mais elle peut également avoir de l'intérêt sans cette option.

Pour créer un nouveau mot de passe d'application, vous devez&nbsp;:
  * cliquer sur votre nom d'utilisateur en haut à droite de l'interface, cliquer sur la ligne [Paramètres](https://framagenda.org/settings/user) :

    ![framagenda-2FA-01](images/framagenda-2FA-01.png)

  * Dans la nouvelle page qui s'affiche, cliquez sur la ligne [Sécurité](https://framagenda.org/settings/user/security) dans le menu latéral gauche :

    ![framagenda-2FA-02](images/framagenda-2FA-02.png)

  * choisir un nom pour l'application, par exemple **Framagenda-doc**, puis cliquer sur **Créer un nouveau mot de passe d'application** :

    ![framagenda-mpd-app-01](images/framagenda-mpd-app-01.png)

  * Par mesure de sécurité confirmer votre mot de passe :

    ![framagenda-mpd-app-02](images/framagenda-mpd-app-02.png)

Le mot de passe s'affiche une seule fois pour que vous le recopiez dans votre application. Si vous avez perdu ce mot de passe, vous devrez recréer un nouveau mot de passe. Votre nom d'utilisateur ne change pas.

![framagenda-mpd-app-03](images/framagenda-mpd-app-03.png)
