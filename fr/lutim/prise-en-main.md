# Prise en main

## Supprimer une photo

Pour supprimer une photo que vous avez versé sur Framapic vous devez, **depuis le même navigateur que celui utilisé pour l'envoi de l'image**&nbsp;:

  1. aller dans l'onglet **Mes images**
  * cliquer sur l'icône <i class="fa fa-trash" aria-hidden="true"></i>

## Modifier un date d'expiration

Pour modifier une date d'expiration vous devez, **depuis le même navigateur que celui utilisé pour l'envoi de l'image**, aller dans l'onglet **Mes images**, puis&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
  * changer la date
  * cliquer sur **Enregistrer les modifications**

![image montrant comment éditer une date](images/lutim_edit_date_fr.png)
