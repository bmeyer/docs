# Framatalk

[Framatalk](https://framatalk.org) est un service en ligne libre qui permet de créer un salon de visio-conférence et d’y inviter son, sa, ses interlocuteur·trice·s.

Propulsé par le logiciel libre [Jitsi Meet](https://jitsi.org/Projects/JitsiMeet), Framatalk offre de nombreuses options&nbsp;:

* Un tchat pour discuter en mode texte (il vous faudra entrer un pseudo)&nbsp;;
* Un bouton d’invitation à la conversation (partage par email de l’adresse web du salon)&nbsp;;
* Des boutons pour activer/désactiver le micro, la caméra, le mode plein écran&nbsp;;
* Un accès aux paramètres (modifier son pseudo, sa caméra, son micro)&nbsp;;
* La possibilité que Framatalk retienne votre profil de paramètres (il créera un cookie)&nbsp;;
* Les droits de modération du salon (pour la première personne arrivée)&nbsp;;
* La possibilité de protéger le salon par mot de passe (pour le modérateur).

---

### Présentation vidéo

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://framatube.org/videos/embed/3fef30e5-a058-4a59-b0a6-14a3a2fd72b8" frameborder="0" allowfullscreen></iframe>

### Pour aller plus loin&nbsp;:

* [Prise en main](prise-en-main.html)
* [Prise en main détaillée](prise-en-main-detaillee.html)
* [Fonctionnalités](fonctionnalites.html)
* [Essayer Framatalk](https://framatalk.org)&nbsp;;
* Application Android&nbsp;:
  * [Jitsi Meet](https://play.google.com/store/apps/details?id=org.jitsi.meet) sur Google Play
* Le [site officiel de Jitsi meet](https://jitsi.org/Projects/JitsiMeet), moteur de Framatalk&nbsp;;
* Participer au [code de Jitsi Meet](https://github.com/jitsi/jitsi-meet)&nbsp;;
* [Installer Jitsi Meet](http://framacloud.org/cultiver-son-jardin/installation-de-jitsi-meet/) sur vos serveurs&nbsp;;

---

* [Prise en main de Mumble](mumble.html)

---

* [Dégooglisons Internet](https://degooglisons-internet.org)&nbsp;;
* [Soutenir Framasoft](https://soutenir.framasoft.org).
