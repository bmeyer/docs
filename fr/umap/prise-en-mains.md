# Prise en mains

## Création d'un compte Framacarte

Pour pouvoir utiliser Framacarte avec un compte et sauvegarder vos cartes en un même endroit, vous devez utiliser ou créer un compte [OpenStreetMap](https://openstreetmap.org/). Pour ce faire, vous devez&nbsp;:

  * aller sur https://framacarte.org/fr/
  * cliquer sur **Connexion / Créer un compte** (**1**)
  * cliquer sur l'icône OpenStreetMap (**2**)
  ![Image créer ou se connecter avec un compte OSM](images/umap_creation_utilisation_compte.png)
  * Vous allez être redirigé⋅e sur le site OpenStreetMap ; vous pouvez soit&nbsp;:
    * vous connecter à un compte déjà existant
    * vous créer un compte en cliquant sur **S'inscrire maintenant**
  * en vous connectant *via* OpenStreetMap, une demande d'autorisation vous sera demandée, cliquez sur **Accorder l'accès**

## Récupérer l'édition d'une carte créée sans compte

Si vous n'avez pas [créé de compte](#création-dun-compte-framacarte), vous aurez besoin d'un lien d'édition secret (de la forme `https://framacarte.org/fr/map/anonymous-edit/SERIE-DE-CHIFFRES-ET-LETTRES`). Celui-ci est à garder précieusement et à utiliser si vous changez de navigateur ou si vous perdez les droits d'édition de votre carte.

Il est affiché lors de votre premier enregistrement et vous pouvez le retrouver&nbsp;:

  1. en cliquant sur <i class="fa fa-pencil" aria-hidden="true" style="background: #4d5759; color: #fff; padding: 5px; border-radius: 3px;"></i>
  * puis en cliquant sur <i class="fa fa-key-modern" aria-hidden="true" style="background: #4d5759; color: #fff; padding: 5px; border-radius: 3px;"></i>
  * en conservant le lien `Lien d'édition secret: https://framacarte.org/fr/map/anonymous-edit/SERIE-DE-CHIFFRES-ET-LETTRES`

En cliquant sur ce lien vous obtenez à nouveau les droits d'édition.

<div class="alert-info alert">En cliquant sur le lien d'édition secret <b>en étant connecté⋅e à votre compte OpenStreetMap</b> vous associerez alors la carte à votre compte</div>

## Créer un marqueur a une position précise (avec la latitude et la longitude)

Pour positionner un marqueur en fonction d'une longitude et d'une latitude, vous devez&nbsp;:

  1. sélectionner le marqueur et le positionner approximativement dans la zone souhaitée
  - cliquer sur le marqueur puis sur l'icône <i class="fa fa-pencil" aria-hidden="true"></i> pour l'éditer
  - cliquer sur **Coordonnées**
  - entrer la Latitude (avec une `,` et non un `.`)
  - entrer la longitude (avec une `,` et non un `.`)


![image gpx lat long umap](images/umap_gpx-lat_long.png)

## Télécharger les données de la carte

Pour télécharger les données de la carte dans le format `gpx` et les réutiliser dans un autre logiciel, vous devez cliquer sur l'icône ![icône partage umap](images/umap_share.png) puis sélectionner `gpx` et cliquer sur **Télécharger les données**.

![Images de téléchargement des données](images/umap_prise_en_main-dl_gpx.png)

## Tracer les contours un département ou une région

Dans cet exemple nous allons tracer les contours du département du Morbihan&nbsp;:

![image du résultat final - contours du Morbihan](images/carte_morbihan_final.png)

Pour cela nous allons utiliser le site https://france-geojson.gregoiredavid.fr/ pour récupérer le fichier `geojson`.

![image de la création du fichier](images/carte_geosjson_selection.png)

  1. on précise qu'on souhaite un département
  * on sélectionne le département
  * on télécharge le fichier au format `geojson`

Une fois téléchargé sur notre ordinateur, il faut aller sur sa carte Framacarte puis&nbsp;:

![image de l'import du calque geojson](images/carte_import_calque_geojson.png)

  1. on clique sur l'icône **Importer des données**
  * on clique sur **Parcourir…** pour récupérer le fichier `departement-56-morbihan.geojson`
  * on choisit le format **geojson**
  * on choisit d'importer dans un nouveau calque
  * on clique sur **Importer**

On se retrouve avec le calque importé&nbsp;:

![image du calque importé](images/carte_calque_importe.png)

Pour le personnaliser (comme par exemple changer la couleur du trait et enlever la couleur de fond ou son opacité) il faut&nbsp;:
  * cliquer sur <i class="fa fa-database" aria-hidden="true"></i>
  * cliquer sur l'icône <i class="fa fa-pencil" aria-hidden="true"></i> sur la ligne du calque
  * cliquer sur **Propriétés de la forme** et modifier les paramètres souhaités
