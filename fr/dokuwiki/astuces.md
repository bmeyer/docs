# Astuces

## Création d'un wiki

Voir [la page **Prise en main** de Framasite / Administration](../framasite/prise-en-main.md#création-dun-wiki).

## Supprimer une page wiki

Pour supprimer une page wiki vous devez supprimer l'intégralité de son contenu et sauvegarder la page.

## Créer une barre de navigation

Pour avoir une barre de navigation latérale, vous devez créer la page `sidebar`. Pour ce faire :

  * allez sur la page principale de votre site (`https://monsite.frama.wiki/`)
  * ajoutez `sidebar` à la fin de l'adresse (`https://monsite.frama.wiki/sidebar`)
  * cliquez sur **DokuWiki Editor**

    ![DokuWiki edtition menu](images/dokuwiki_edition_sidebar.png)

  * ajoutez ce que vous souhaitez mettre dans la barre de navigation - par exemple :
    ```
    * [[https://framasoft.org|Framasoft]]
    * [[https://docs.framasoft.org/fr/|Documentation Framasoft]]
    * [[https://contributopia.org/|Contributopia]]
    ```
  * cliquez sur **Enregistrer**

  **Avant sidebar** :

  ![page avant sidebar](images/dokuwiki_page_avant_sidebar.png)

  **Après sidebar** :

  ![page après sidebar](images/dokuwiki_page_apres_sidebar.png)

## Modifier le titre et la description

Pour modifier le titre et/ou la description de son wiki, il faut :

* cliquer sur **Administrer**
* cliquer sur **Paramètres de configuration**
* modifier **Titre du wiki (nom du wiki)** ou **Descriptif du site (si le modèle supporte cette fonctionnalité)**
* cliquer sur **Enregistrer** (tout en bas de la page)

## Créer un wiki privé

Pour créer un wiki visible et/ou modifiable uniquement par des utilisateurs, vous devez bloquer l'accès aux personnes non authentifiées. Pour ce faire, vous devez soit sélectionner **Wiki public (lecture pour tout le monde, écriture et envoi de fichiers pour les utilisateurs enregistrés)** pour la **Gestion des contrôles d'accès** à la création du wiki, ou alors, une fois celui-ci créé, vous devez :

  * cliquer sur **Administrer**
  * cliquer sur **Gestion de la liste des contrôles d'accès (ACL)**
  * si **@All** n’existe pas :
    * vérifier que **[racine]** soit surligné en jaune
    * sélectionner **@All** dans **Autorisations pour**
    * sélectionner **Aucune**
    * cliquer sur **Mettre à jour**

    ![wiki gestion ACL](images/dokuwiki_gestion_acl.png)

  * si **@All** est dans la liste, le passer à **Aucune** dans **Autorisations** et cliquer sur **Mettre à jour**

    ![ wiki ACL](images/dokuwiki_acl.png)

Il faudra alors être connecté pour voir le contenu.
