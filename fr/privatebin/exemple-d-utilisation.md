# Exemple d'utilisation

## Exemple simple

![images UI framabin](images/bin_ui.png)

Dans le cas d'une utilisation rapide comme celle d'envoyer un mot de passe, vous devez&nbsp;:

  * entrer le texte dans la zone de texte (**1**)
  * **optionnellement** indiquer si vous souhaitez que le Framabin soit effacé après sa première lecture (**2**) ou encore mettre un mot de passe pour pouvoir afficher le texte
  ![bin options](images/bin_options.png)
  * **optionnellement** indiquer une période autre que 1 semaine (**3**)
  * cliquer sur **Envoyer** (**4**)

Il suffira alors d'envoyer le lien obtenu à la personne avec qui vous souhaitez partager l'information.

![image du résultat](images/bin_lien.png)
