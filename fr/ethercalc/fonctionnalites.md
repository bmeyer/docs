# Fonctionnalités

## Intégrer Framacalc sur votre site

[Framacalc](http://www.framacalc.org/) propose un tableur en ligne, accessible à tous. Les adresses ne sont pas indexées par les moteurs de recherche, mais il n'y a pas la possibilité de verrouiller un Calc pour en rendre son accès limité aux personnes autorisées. Toute personne disposant de l'adresse du Calc pourra le modifier à sa convenance.

Il est possible, pour s'assurer une limitation plus importante, d'intégrer via un iframe le Framacalc sur la page de votre site web, ou via un Wiki, par exemple. Vous pourrez alors gérer les accès à cette page contenant l'iframe, et la modification sera faite via cette page. L'adresse originale du Framacalc n'apparaissant pas, les risques d'avoir des modifications non désirées sont moindre. Vous trouverez ci-dessous la syntaxe à utiliser pour réaliser une solution de ce type.

### Calc accessible en lecture/écriture

````
<iframe src="http://framacalc.org/CalcDeTest2" width="660" height="480" frameborder=0 ></iframe>
````

**Syntaxe pour Dokuwiki**

````
<HTML>
<iframe src="http://framacalc.org/CalcDeTest2" width="660" height="480" frameborder=0 ></iframe>
</HTML>
````

### Calc accessible en lecture seule

````
<iframe src="http://framacalc.org/_/CalcDeTest2/html" width="660" height="480" frameborder="0" ></iframe>
````

**Syntaxe pour Dokuwiki**

````
<HTML>
<iframe src="http://framacalc.org/_/CalcDeTest2/html" width="660" height="480" frameborder="0" ></iframe>
</HTML>
````

### Figer la première ligne/colonne d’un tableur

En haut de la barre de défilement verticale pour les lignes et à gauche de la barre de défilement horizontale pour les colonnes se trouvent des boutons bleus ![bouton verrouillage vertical](images/sc_paneslider-v.gif) ![bouton verrouillage horizontal](images/sc_paneslider-h.gif) qui permettent de définir la zone d’en-tête à garder visible. Il faut juste glisser/déposer le bouton à l’endroit voulu comme sur un tableur classique.

### Impression d'un calc

Cliquez sur la disquette ![disquette](images/download.png) dans le coin en haut à gauche du tableau (à gauche de la colonne A), exportez le calc au format `html` et utilisez enfin la fonction « Imprimer » de votre navigateur web.

### Mettre en forme mon contenu (bordures, couleurs, etc.)

  1. Commencez par sélectionner toutes les cellules à mettre en forme
  2. Cliquez sur l’onglet **Format**
  3. Appliquez le format voulu (une fenêtre de visualisation de rendu vous montre le rendu final en temps réel)
  4. IMPORTANT : cliquez sur **Enregistrer les paramètres de : (vos cellules)**, en haut à gauche.

### Fusionner des cellules

Pour fusionner deux (ou plusieurs) cellules vous devez&nbsp;:

  * cliquer sur la première cellule
  * maintenir la touche majuscule de votre clavier (<i class="fa fa-long-arrow-up" aria-hidden="true"></i>)
  * cliquer sur la seconde cellule
  * cliquer sur ![image de l'icône de fusion des cellules](images/merge_unmerge_cells.png)

Pour séparer les cellules il suffit de la sélectionner et de cliquer à nouveau sur ![image de l'icône de fusion des cellules](images/merge_unmerge_cells.png)

### Verrouiller / déverrouiller une cellule

Sélectionnez la ou les cellules que vous souhaitez verrouiller et cliquez sur ![image icone verrouiller clac](images/sc_lock.png). Pour déverrouiller, sélectionnez la ou les cellules et cliquez sur ![image icone déverrouiller clac](images/sc_unlock.png).

### Nommer une cellule pour l’utiliser dans une formule

Pour utiliser le contenu d’une cellule afin de la réutiliser dans une formule, vous devez :

  * cliquer sur l’onglet **Noms**
  * lui assigner un nom et une valeur
  * enregistrer
  ![initialisation de la cellule](images/initialisation.png)
  ![utilisation de la cellule](images/utilisation_cellule_framacalc.png)

Pour faire référence à une cellule d’un autre onglet, vous devez utiliser une formule de type `=foo!A1` où `foo` est le nom de l’onglet qui contient la case `A1`.

### Copier les données et la mise en forme d’un onglet vers un autre

Pour copier un onglet ou les données d’un calc à un autre, il faut utiliser le presse papier avec le format SocialCalc. Le procédé est un peu laborieux :

  1. Sélectionner les cellules
  2. Cliquer sur l’icône **Copier**
  3. Aller dans l’onglet **Presse-papier**
  4. Choisir **Format de sauvegarde de SocialCalc**
  5. Sélectionner tout le texte qui s’affiche dans le cadre en dessous (ce qui commence par **version :…**)
  6. Le copier `CTRL+C`
  7. Aller dans la feuille de destination
  8. Choisir la cellule de destination
  9. Aller dans l’onglet **Presse-papier** (chaque feuille a son presse-papier)
  10. Choisir **Format de sauvegarde de SocialCalc**
  11. Coller le texte `CTRL+V`
  12. Cliquer sur **Copier le presse-papier de SocialCalc avec ceci** (afin de le mettre dans le presse papier de la feuille de destination)
  13. Cliquer sur **Édition**
  14. Cliquer sur l’icône **Coller**

### Tri par ordre alphabétique

Il est possible de trier vos colonnes `A` et `B` en fonction de l'ordre alphabétique de `A` :

  1. Sélectionnez les cases que vous souhaitez mettre en ordre
  - Cliquez sur **Trier**
  - Vérifiez **Définir les cellules à trier** affiche bien les cases voulues
  - Cliquez sur le bouton `OK` à côté
  - Cliquez sur `Trier A1:B3` (`A1:B3` pour cet exemple)
  ![calc onglet tri](images/calc_tri.png)

Vous passerez alors de :

![calc avant tri](images/calc_avant_tri.png)

à :

![calc après tri](images/calc_apres_tri.png)

### Faire une liste déroulante

Pour créer une liste déroulante vous devez mettre le code
```
=SELECT("proposition par défaut","proposition par défaut,proposition 2,proposition 3")
```

Pour obtenir ![calc liste déroulante exemple](images/calc_liste_deroulante_exemple.png) le code sera ![calc liste déroulante code](images/calc_liste_deroulante_code.png)

### Insérer une image dans une cellule

Pour cela il faut utiliser le <abbr title="Hypertext Markup Language">HTML</abbr> et une image **déjà en ligne** (vous ne pouvez pas envoyer d'image dans Framacalc).

Si vous souhaitez utiliser le logo Framasoft dans une cellule, il faut :

  * récupérer son adresse (https://framasoft.org/nav/img/logo.png) en faisant un `clic droit` dessus, puis `Copier l'adresse de l'image`
  * mettre le lien avec la syntaxe HTML dans la cellule souhaitée :
  ```
  <img src="https://framasoft.org/nav/img/logo.png"/>
  ```
   (il faut remplacer l'adresse entre guillemets par celle que vous souhaitez)
  * cliquer sur l'onglet **Format**
  * changer **Texte** de **Défaut** à **HTML**
  * cliquer sur **Enregistrer les paramètres de : votre cellule**

La cellule affichera alors l'image.

### Faire un retour à la ligne

Pour faire un retour à la ligne vous devez changer le format par défaut du calc ou de la cellule.

Pour ce faire, vous devez&nbsp;:

  1. cliquer sur l'onglet **Format**
  * cliquer sur **Afficher les paramètres de la feuille** (ou, si vous souhaitez appliquer les changements sur **une** cellule vous devez cliquer dessus avant l'étape 1 et ne pas cliquer sur **Afficher les paramètres de la feuille**)
  * cliquer sur **Défaut** sous **Texte**
  * choisir **HTML** dans la liste
  * cliquer sur **Enregistrer**
  ![gif animé montrant comment modifier la case](images/calc_html.gif)

A partir de ce moment vos cellules seront toutes au format HTML. Pour revenir à la ligne vous devez alors ajouter `<br />` là où vous souhaitez mettre un retour à la ligne. Par exemple `'Ligne 1<br />Ligne 2 <br />Ligne 3` donnera&nbsp;:

![image montrant le résultat](images/calc_html_resultat.png)

### Version non modifiable

Si vous souhaitez donner une version **non modifiable** (en lecture seule) d'un calc, vous devez ajouter **/view** à son adresse. Par exemple, le calc de test https://lite.framacalc.org/CalcDeTest devient, en lecture seule : https://lite.framacalc.org/CalcDeTest/view

### Restaurer une version

Si vous souhaitez revenir à une version de votre calc (à cause d'erreurs ou de suppression du contenu de celui--ci par exemple), vous devez&nbsp;:

  1. cliquer sur l'onglet **Anciennes révisions**
  * cliquer sur la version à restaurer
  * cliquer sur l'icône

    ![image procédure de restauration](images/calc_restauration_version.png)

  * cliquer **OUI** à la question **Replacer la version actuelle de […]**

### Utiliser la valeur d'une cellule d'une autre feuille de calc

Vous avez la possibilité de récupérer la valeur d'une cellule d'une feuille de calc pour l'afficher dans un autre calc. Il y a cependant deux contraintes&nbsp;:

  * vous devez être sur la même instance calc. Nous en avons deux : **lite**.framacalc.org et framacalc.org (pour les calcs anciens)
  * les noms des calcs (ce qui est après `framacalc.org/`) **ne** doivent **pas** comporter de majuscules (ce n'est plus le cas pour les nouveaux calcs crées)

Ensuite, vous devez utiliser la formule suivante&nbsp;: `=feuille1!cellule`

Si vous voulez intégrer la valeur de la feuille2, cellule C4 dans la feuille1, vous devez donc, **dans la feuille1** écrire : `=feuille2!c4`
