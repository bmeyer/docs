Gérer les appareils et les navigateurs connectés
=====================================

La page des paramètres personnels permet d'avoir une vue d'ensemble des navigateurs et des appareils connectés.

Gérer les navigateurs connectés
---------------------------

Dans la liste des navigateurs connectés, vous pouvez voir quels navigateurs se sont connectés à votre compte récemment :

![](images/settings_sessions.png)


Vous pouvez utiliser l’icône de la corbeille pour déconnecter l'un des navigateurs de la liste.

Gérer les appareils
----------------

Dans la liste des appareils connectés, vous pouvez voir tous les appareils et clients pour lesquels vous avez généré un mot de passe spécifique, ainsi que leurs activités récentes :

![](images/settings_devices.png)


Vous pouvez utiliser l'icône de la corbeille pour déconnecter n'importe quel appareil de la liste.

Au bas de la liste vous trouverez un bouton pour créer un nouveau mot de passe spécifique à un appareil. Vous pouvez choisir un nom pour identifier le jeton d'accès plus tard. Le mot de passe généré sera utilisé pour configurer le nouveau client.

Il est conseillé de créer un jeton d'accès spécifique pour chaque appareil que vous connectez à votre compte, de manière à pouvoir déconnecter chacun d'eux séparément si nécessaire ;

![](images/settings_devices_add.png)

<div class="alert alert-info">Vous n'avez accès au mot de passe de l'appareil qu'au moment de sa création. Nextcloud n'enregistrera pas le mot de passe en clair, c'est pourquoi il est recommandé de saisir immédiatement le mot de passe sur le nouveau client.</div>

> **note**
>
> Si vous prévoyez d'utiliser [l'option 2 facteurs d'authentification](User_2fa.md) pour votre compte, la configuration des clients n'est possible qu'en utilisant des mots de passe spécifiques par appareil. Le client refusera la connexion de clients qui utiliseraient vos identifiants.
