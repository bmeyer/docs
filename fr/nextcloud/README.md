# <b class="frama">Frama</b><b class="vert">drive</b>

Framadrive est un service en ligne d’hébergement de fichiers.

En créant un compte, vous disposez de 2Go de stockage en ligne synchronisés entre vos ordinateurs, tablettes, mobiles… ([comment faire ?](https://framadrive.org/login#TutoSync)) et que vous pouvez partager facilement avec vos contacts.

Framadrive repose sur le logiciel libre [Nextcloud](https://nextcloud.com/). Les données sont hébergées sur les serveurs de Framasoft.

Nextcloud est un logiciel libre de synchronisation et de partage de fichiers pour tous, du particulier utilisant le serveur Nextcloud libre au sein de son foyer en toute intimité, à la grande entreprise ou au fournisseur de services utilisant Nextcloud Enterprise Subscription. Nextcloud fournit une solution logicielle sûre, sécurisée et conforme aux normes pour synchroniser et partager des fichiers sur des serveurs que vous contrôlez.

Vous pouvez partager un ou plusieurs fichiers et dossiers sur votre ordinateur et les synchroniser avec votre serveur Nextcloud. Placez des fichiers dans vos dossiers partagés locaux et ces fichiers seront immédiatement synchronisés avec le serveur ainsi qu'avec tout autre appareil utilisant l'application Nextcloud / ownCloud Desktop Sync Client ou les applications mobiles Android ou iOS.

---

## Tutoriel vidéo

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://framatube.org/videos/embed/b584d173-4b2f-494c-9669-5495324837f4" frameborder="0" allowfullscreen></iframe>

Pour vous aider dans l’utilisation du logiciel, voici un tutoriel vidéo réalisé par [arpinux](http://arpinux.org/), artisan paysagiste de la distribution GNU/Linux pour débutant [HandyLinux](https://handylinux.org/).

---

### Pour aller plus loin&nbsp;:

  * Essayer [Framadrive](https://framadrive.org)
  * [Prise en main](prise-en-main.md)
  * [Synchronisation des données](synchro_clients.md)
  * Le [site officiel](https://nextcloud.com/) de Nexcloud (à soutenir&nbsp;!)
  * [Participer au code](https://github.com/nextcloud) de Nextcloud
  * Installer [Nextcloud sur vos serveurs](http://framacloud.org/cultiver-son-jardin/installation-de-nextcloud/)
  * [Dégooglisons Internet](https://degooglisons-internet.org/)
  * [Soutenir Framasoft](https://soutenir.framasoft.org/)
