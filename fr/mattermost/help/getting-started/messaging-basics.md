# Bases de la messagerie


_____

**Écrivez un message** en utilisant la boîte de texte en bas de Mattermost. Appuyez sur **ENTRÉE** pour envoyer un message. Utilisez **Majuscule + ENTRÉE** pour créer une nouvelle ligne sans envoyer de message.

**Répondez à un message** en cliquant sur la flèche de réponse à côté du texte du message.

![reply arrow](../../images/replyIcon.png)

**Notifiez vos coéquipiers** quand c'est nécessaire en saisissant `@pseudo`

**Formatez vos messages** en utilisant Markdown, qui prend en charge la mise en forme du texte, les titres, les liens, les émoticônes, les blocs de code, les citations, les tableaux, les listes et les images sur la même ligne.


| Texte entré                                                | Aperçu                                                   |
|------------------------------------------------------------|:---------------------------------------------------------|
| `**gras**`                                                 | **gras**                                                 |
|`*italique*`                                                | *italique*                                               |
| `~~texte barré~~`                                          | ~~texte barré~~                                          |
| `[Framateam](https://framateam.org)`                       | [Framateam](https://framateam.org)                       |
| `![image insérée](https://framasoft.org/nav/img/logo.png)` | ![image insérée](https://framasoft.org/nav/img/logo.png) |


**Joignez des fichiers** en les glissant-déposant dans Mattermost ou en cliquant sur l'icône de pièce jointe à côté de la boîte de texte.

<p class="alert alert-warning">
Pour des pièces jointes temporaires, préférez un service comme <a href="https://framapic.org/">Framapic</a> ou <a href="https://framadrop.org/">Framadrop</a> pour éviter de prendre inutilement de la place sur nos serveurs :)
</p>

En apprendre plus sur :

- [Écrire des messages et des réponses](http://docs.framateam.org/help/messaging/sending-messages.html)
- [Lire les messages](http://docs.framateam.org/help/messaging/reading-messages.html)
- [Mentionner ses coéquipiers](http://docs.framateam.org/help/messaging/mentioning-teammates.html)
- [Formater ses messages en utilisant Markdown](http://docs.framateam.org/help/messaging/formatting-text.html)
- [Joindre des fichiers](http://docs.framateam.org/help/messaging/attaching-files.html)
