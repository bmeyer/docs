Gestion des utilisateurs
========================

Ajouter un nouvel utilisateur
-----------------------------

Pour ajouter un nouvel utilisateur, vous devez être administrateur.

1. Depuis le menu déroulant situé en haut à droite, cliquez sur **Gestion des utilisateurs**
2. Dans la partie haute vous avez un lien **Créer un utilisateur local** ou **Créer un utilisateur distant**
3. Informez les champs de saisie et enregistrez

![Nouvel utilisateur](screenshots/new-user.png)

Quand vous créez un **utilisateur local**, vous devez préciser au moins deux valeurs :

- **nom d'utilisateur** : c'est l'identifiant unique de votre utilisateur (login)
- **mot de passe** : le mot de passe de votre utilisateur doit comporter au moins 6 caractères

Pour les **utilisateurs distants**, seul le nom d'utilisateur est obligatoire.

Modifier des utilisateurs
-------------------------

Quand vous allez au menu **utilisateurs**, vous disposez d'une liste d'utilisateurs. Pour modifier un utilisateur cliquez sur le lien **Modifier**.

- si vous êtes un utilisateur ordinaire, vous ne pouvez modifier que votre propre profil
- vous devez être administrateur pour pouvoir modifier n'importe quel utilisateur

Supprimer des utilisateurs
--------------------------

Depuis le menu **utilisateurs**, cliquez sur le lien **supprimer**. Ce lien n'est visible que si vous êtes administrateur.

Si vous supprimez un utilisateur particulier, **les tâches assignées à cette personne ne lui seront plus assignées** après cette opération.

Ajouter des utilisateurs à un projet
------------------------------------

Pour ajouter un utilisateur sur un projet vous devez cliquer sur **Menu** > **Préférences** > **Permissions**. De là, entrez le nom de l'utilisateur déjà créé, son nom apparaîtra dans une liste, et cliquez dessus.

Vous pouvez choisir de lui donner le rôle **Membre du projet**, **Chef de projet** ou **Visualiseur de projet** (voir la section [Rôles](roles.html#rôles-au-niveau-des-projets))
