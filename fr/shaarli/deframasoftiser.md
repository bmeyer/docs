# Déframasoftiser MyFrama

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Exporter

Pour exporter vos liens, vous devez&nbsp;:

  * cliquer sur l'icône <i class="fa fa-cog" aria-hidden="true"></i> dans la barre de navigation supérieure
  * cliquer sur <i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i> **Exporter**
  * sélectionner ce que vous souhaitez exporter&nbsp;:
    * **Tout** pour tous les liens enregistrés
    * **Privés** pour les liens privés
    * **Publics** pour les liens publics
  * cliquer sur le bouton **Exporter**

## Importer

Pour importer vos liens dans votre nouvelle instance, vous devez&nbsp;:

  * cliquer sur l'icône <i class="fa fa-cog" aria-hidden="true"></i> dans la barre de navigation supérieure
  * cliquer sur <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i> **Importer**
  * cliquer sur le bouton **Parcourir…** pour récupérer l'export
  * **Optionnel** :
    * **Importer tous les liens comme privés** pour passer vos éventuels liens publics en privés sur votre nouvelle instance
    * **Remplacer les liens existants** supprimera les liens présents sur votre compte par le fichier export
  * cliquer sur le bouton **Importer**

## Supprimer son compte MyFrama

Il n’est pas possible pour les utilisatrices et utilisateurs de supprimer leur compte. Il faut [nous en faire la demande](https://contact.framasoft.org/#myframa) **depuis l’adresse mail associée au compte MyFrama** et **en nous précisant le nom de votre compte**.
