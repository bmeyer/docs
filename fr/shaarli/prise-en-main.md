# Prise en main

## Partage

### Avec le Marque-page

Vous pouvez ajouter le marque-page à votre navigateur pour pouvoir ajouter la page sur laquelle vous êtes, facilement. Pour cela, vous devez glisser/déposer (c'est-à-dire maintenir le clic sur le bouton et le déplacer dans la barre personnelle puis lâcher - voir la vidéo ci-dessous) le bouton **Partager le lien** de vos paramètres dans votre barre personnelle. Ensuite, vous n'avez plus qu'à cliquer sur ce marque-page pour ouvrir la page de partage de lien.

<div class="embed-responsive embed-responsive-4by3">
  <video controls="controls" preload="none" muted
    class="embed-responsive-item"
    width="640" height="480">
    <source src="images/myframa-bookmarklet.mp4" type="video/mp4">
  </video>
</div>
