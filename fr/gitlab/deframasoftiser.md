# Déframasoftiser Framagit

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

Il n'est pas possible de migrer un compte dans son intégralité mais il est possible de migrer vos projets.


## Exporter

Sera exporté&nbsp;:

  * Dépôts de projet et de wiki
  * Téléchargement de projetConfiguration du projet, y compris les services
  * Sujets avec commentaires, demandes de fusion avec diffs et commentaires, étiquettes, jalons, snippets et autres entités du projet
  * LFS objets
  * Tableaux des tickets

**Ne sera pas** exporté&nbsp;:

  * Journaux des tâches et artefacts
  * Images du registre des conteneurs
  * Variables CI
  * Webhooks
  * Tout jeton chiffré

Pour exporter votre projet, vous devez&nbsp;:

  * cliquer sur **Pramètres** dans la barre latérale sur la page de votre projet
  * cliquer sur le bouton **Étendre** dans la section **Advanced**
  * cliquer sur le bouton **Export project** dans la section du même nom

Une fois que le fichier exporté est prêt, vous recevrez un courriel de notification avec un lien de téléchargement, ou vous pouvez le télécharger à partir de la section **Export project**.

## Importer

Pour importer un projet, vous devez&nbsp;:

  * créer un nouveau projet
  * cliquer sur l'onglet **Import project**
  * cliquer sur **GitLab export** dans la section **Import project from**
  * donner un nom de projet et un identifiant
  * cliquer sur le bouton **Parcourir…**
  * récupérer le fichier exporté (voir ci-dessus)
  * cliquer sur **Importer un projet**

## Supprimer un compte

Une fois vos projets exportés (et éventuellement importés), vous pouvez supprimer votre compte. Pour cela, vous devez&nbsp;:

  * votre avatar (en haut à droite de la page)
  * cliquer sur **Paramètres**
  * cliquer sur **Compte** dans la barre latérale gauche
  * cliquer sur **Supprimer le compte**
