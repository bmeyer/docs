# Framabag

Framabag vous permet de mettre de côté les articles que vous n’avez pas le temps de lire tout de suite.

Framabag repose sur le logiciel libre [Wallabag](https://wallabag.org/fr)

[![](images/wallabag.png)](https://wallabag.org/fr)

Pour vous expliquer le principe, voici une présentation de Pyves réalisée avec le logiciel RevealJS et les dessins de [Gee](http://ptilouk.net/) sous licence CC-By-SA.

<div class="embed-responsive embed-responsive-16by9">
  <iframe src="https://framabag.org/cquoi/" class="embed-responsive-item" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
</div>

---

La documentation ci-dessous provient du site officiel de [Wallabag](http://doc.wallabag.org/fr/master/).

## Table des matières
  * [Déframasoftiser Internet](deframasoftiser.html)
  * [Se créer un compte](user/create_account.html)
  * [Configuration](user/configuration.html)
  * [Migrer depuis…](user/import.html)
  * [Articles](user/articles.html)
  * [Erreurs durant la récupération des articles](user/errors_during_fetching.html)
  * [Retrouver des articles grâce aux filtres](user/filters.html)
  * [Configurer les applications mobile pour wallabag](user/configuring_mobile.html)
  * [Application Android](user/android.html)
