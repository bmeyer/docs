# Déframasoftiser Framasite «&nbsp;Blog&nbsp;»

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Export du site

Pour exporter votre site, vous devez&nbsp;:
  1. aller sur la page **Backup**
  * cliquer sur le bouton **BACKUP**
  * cliquer sur **Télécharger la sauvegarde** ou cliquer sur le lien tout en haut de la liste de **Latest backups**

![image de la partie sauvegarde de Grav](images/deframasoftiser-grav.png)
