<p class="alert alert-warning">La documentation est en cours de rédaction.<br>
Si vous souhaitez apporter des compléments, vous pouvez contribuer
sur <a href="https://framagit.org/framasoft/docs">le dépôt git</a>,
soit en ouvrant un <a href="https://framagit.org/framasoft/docs/issues">ticket</a>
décrivant les modifications, soit en proposant une
<a href="https://framagit.org/framasoft/docs/merge_requests">demande de fusion</a>.</p>

# Prise en main

## Gestion des images

### Images globales

Le dossier `/images` contient l’ensemble des images communes à tout le site.

Il sert de référence pour les modules et notamment le module <code>images-collage</code>
ce qui permet d’éviter d’avoir à saisir l’ensemble du chemin d’accès aux images ;
le nom du fichier seul suffit.

Dans d’autres contextes il faudra utiliser la syntaxe markdown
<pre><code>![](/images/une-image.jpg)</code></pre>

L’adresse de l’image sera du genre :

    https://demo.frama.site/user/pages/images/une-image.jpg

<p class="alert alert-info">
Pour uploader une image, il suffit de la glisser-déposer
(ou de cliquer pour ouvrir l’explorateur de fichiers) avec la souris
dans le cadre gris « Médias de la page ».
</p>

### Images relatives

Pour les pages et articles, il est possible d’ajouter des images
directement liées au contenu de l’article.
Pour pouvoir uploader les fichiers, il faut que l’article ou la page
ait déjà été enregistré (sans nécessairement le publier).

La première image ajoutée sert systématiquement d’image « à la une »
pour les articles de blog.

Pour ajouter une image dans le corps du texte, il faut cliquer sur la zone **Médias de la page**
et sélectionner un image sur votre ordinateur&nbsp;:

![image zone médias](images/grav_zone_medias.png)

puis passez simplement la souris sur l’image et
cliquez sur le bouton <i class="fa fa-plus-circle"></i> à sa droite.
Un code markdown de ce genre devrait être inséré :
<pre><code>![](une-image.jpg)</code></pre>

![gif des options images](images/grav_img_icones.gif)

Quand l’article sera publié l’adresse de l’image sera du genre :

    https://demo.frama.site/user/pages/01.page/article/une-image.jpg

### Modifier l’image d’entête

Voici ci-dessous une vidéo illustrant comment, par exemple, modifier l’image d’entête
sur une page avec une « navbar interne ».<br>
L’entête est défini dans le module « header ».<br>
L’image par défaut `bg_home.jpg` est une « image globale ».
qui se trouve donc dans le dossier `/images`.

<div class="embed-responsive embed-responsive-4by3">
  <video controls="controls" preload="none" muted
    poster="images/image-entete.jpg"
    class="embed-responsive-item"
    width="640" height="480">
    <source src="images/image-entete.mp4" type="video/mp4">
  </video>
</div>

### Redimensionner une image

Grav permet de redimensionner les images au cas par cas,
il suffit pour ça d’ajouter `?cropResize=<largeur>,<hauteur>` après le nom du fichier :
<pre><code>![](une-image.jpg?cropResize=640,360)</code></pre>

Mais vous pouvez aussi bien garder l’image originale et lui ajouter la classe CSS `img-responsive`
pour que sa taille s’adapte selon la résolution d’écran de vos visiteurs :
<pre><code>![](une-image.jpg) {.img-responsive}</code></pre>

### Ajouter un élément sonore

Pour ajouter un lecteur audio, vous devez :

  1. Déposer votre fichier dans la zone prévue à cet effet (**Déposez vos fichiers ici ou cliquez dans cette zone** en bas de votre page)
  - Cliquer sur l'icône **Vue** : le fichier s'ouvrira dans un nouvel onglet
  ![vue média](images/grav_media_vue.png)
  - Récupérer l'adresse du média (`https://votre-site.frama.site/user/pages/04-page/Bazingscratch.mp3`)
  - Revenir sur l'onglet de l'édition de votre site
  - Mettre dans votre contenu :
  ```
  <audio controls>
  <source src="https://votre-site.frama.site/user/pages/04-page/Bazingscratch.mp3">
  </audio>
  ```
  - Enregistrer

Vous obtiendrez un lecteur comme ci-dessous :

![lecteur audio](images/lecteur_audio.png)

## Ajouter une vidéo

Pour ajouter une vidéo venant d'un site comme youtube, dailymotion, viméo etc… Vous devez utiliser le code d'intégration d'une vidéo pour la mettre sur votre site.

### Peertube

Si je veux intégrer la vidéo [Contributopia : Peut-on faire du libre sans vision politique ? — Pierre-Yves Gosset](https://framatube.org/videos/watch/7e261f9e-242c-4100-a0bd-268dab321114) présente sur Framatube, je dois&nbsp;:
  * cliquer sur **Partager**
  * copier la ligne sous **Intégration** ; dans notre exemple&nbsp;:

    ```
    <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://framatube.org/videos/embed/7e261f9e-242c-4100-a0bd-268dab321114" frameborder="0" allowfullscreen></iframe>
    ```

  * coller la ligne ci-dessus dans le contenu de votre site
  * enregistrer

### Youtube

Si je veux intégrer la vidéo [PeerTube : préparer l'alternative à Youtube - HS - Monsieur Bidouille](https://www.youtube.com/watch?v=ouvTudXsNtM) de Youtube, je dois&nbsp;:
  * cliquer sur **Partager**
  * cliquer sur **Intégrer**
  * copier le code ; dans notre exemple :

  ```
  <iframe width="560" height="315" src="https://www.youtube.com/embed/ouvTudXsNtM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  ```

  * coller ce code dans le contenu de votre site
  * enregistrer

## Utilisation des templates

### Créer un second blog

Le logiciel utilisé pour les blogs Framasite (http://getgrav.org/) n'étant pas prévu, à la base, pour accueillir deux systèmes de blog sur un même site, vous devez "ruser" en éditant en mode expert votre page.

Commencez par créer une page avec le template `Blog` :

![Ajouter une page blog](images/ajouter-une-page-blog.png)

En passant en mode expert, vous n'aurez pas tous les éléments d'un template blog :

![grav édition en mode expert](images/grav-mode-expert-blog.png)

Il faudra ajouter sous `title` (voir `2`) le bloc suivant, en modifiant les deux champs `Mon second blog !` par la description voulue et `slug` par la valeur de **Nom du dossier** ci-dessus - ici `second-blog` :

```
metadata:
    description: 'Mon second blog !'
slug: second-blog
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: 5
    pagination: true
feed:
    description: 'Mon second blog !'
    limit: 10
pagination: true
```

En cliquant sur **Enregistrer** vous aurez alors un second blog sur lequel mettre des articles dans l'ordre chronologique.

Vous pourrez alors [ajouter une page](creation-de-page.html) avec comme page parente celle que vous venez de créer (ici **Second blog**) :

![cration d'une page blog pour le second blog](images/grav-ajout-page-blog.png)

## Personnalisation du site

### La barre de navigation et le pied de page
Excepté pour les `page_navbar_interne`, la barre de navigation et
le pied de page sont définis dans la page `common`.

<p class="alert alert-danger">Lorsque vous modifiez cette page, il est important de bien faire attention
que les shortcodes soient correctement fermés.<br>
S’il manque une balise de fermeture, le site ainsi que l’espace admin
risquent d’être complètement cassés.</p>

Le menu principal contient la liste des pages de premier niveau.

Le titre « Framasite » est défini dans l’attribut `brand_text`.
Les icônes de réseau sociaux en haut à droite correspondent aux shortcodes `[g-link]`
dont la syntaxe est décrite dans les [composants de base](composants-de-base.html#ic%C3%B4nes-et-liens).

Voici une vidéo illustrant comment personnaliser tout ça :

<div class="embed-responsive embed-responsive-4by3">
  <video controls="controls" preload="none" muted
    poster="images/gravstrap-navbar-footer.jpg"
    class="embed-responsive-item"
    width="640" height="480">
    <source src="images/gravstrap-navbar-footer.mp4" type="video/mp4">
  </video>
</div>

### Modifier les icônes de médias sociaux

Vous devez vous rendre dans la page **common** et modifier/ajouter des `[g-link url` dans le bloc `[g-navbar-menu name=menu-icon icon_type="fontawesome" alignment="right" ]`.

Pour ajouter une icône vous devez la piocher dans les icônes [Font Awesome](https://fontawesome.com/icons?d=gallery).

Si vous souhaitez mettre un lien vers votre groupe Framavox, par exemple, vous devez ajouter&nbsp;:

```
[g-link url="https://framavox.org" icon_type="fontawesome" icon="bullhorn"][/g-link]
```

  1. `[g-link url="https://framavox.org"` lien vers Framavox
  * `icon_type="fontawesome"` pour dire d'utiliser les icônes Font Awesome
  * `icon="bullhorn"]` pour dire d'utiliser l'icône utilisée par Framavox (`fas fa-bullhorn` sur https://fontawesome.com/icons/bullhorn?style=solid : nous ne gardons que le nom de l'icône sans les `fa`)
  * `[/g-link]` pour fermer le shortcode

Un bloc complet pourra donner&nbsp;:

```
[g-navbar-menu name=menu-icon icon_type="fontawesome" alignment="right" ]
    [g-link url="https://framasphere.org/u/framasoft" icon_type="fontawesome" icon="asterisk"][/g-link]
    [g-link url="https://framagit.org/framasoft/framasite" icon_type="fontawesome" icon="git"][/g-link]
    [g-link url="https://framavox.org" icon_type="fontawesome" icon="bullhorn"][/g-link]
[/g-navbar-menu]
```

### Ajouter un ou des sous-menu(s)

Pour ajouter un sous-menu comme ci-dessous&nbsp;:

![image d'un sous menu grav](images/grav_sous-menu.png)

Vous devez ajouter `submenu="nom de la page"`. Par exmple, si je veux ajouter les sous-pages de **Autre**&nbsp;: `submenu="autre"` Si je veux ajouter une ou plusieurs autres pages, je dois mettre leur nom séparé par une virgule&nbsp;: `submenu="page_simple,autre"` (le nom de la page se trouve dans l'onglet **Avancé** de celle-ci).

Exemple complet&nbsp;:

```
[g-navbar-menu name=menu0 alignment="center" submenu="page_simple,autre" attributes="class:highdensity-menu"][/g-navbar-menu]
```

### Ajouter des styles

Pour personnaliser l’apparence du site, il faut utiliser une feuille de style CSS.

Rendez-vous dans `Plugins > CustomCSS` et collez dans le champ `Inline CSS` votre feuille de style.

Illustration en vidéo avec un thème noir/orange dont
[la feuille de style est celle-ci](images/grav-dark.css).

<div class="embed-responsive embed-responsive-4by3">
  <video controls="controls" preload="none" muted
    poster="images/grav-customcss.jpg"
    class="embed-responsive-item"
    width="640" height="480">
    <source src="images/grav-customcss.mp4" type="video/mp4">
  </video>
</div>

Un jeu de thème vous est également proposé en dessous de la zone de texte.<br>
En cliquant sur le bouton <i class="fa fa-plus-circle" aria-hidden="true"></i>
<span class="sr-only">Utiliser</span>, le champ sera rempli avec la feuille de style adéquate.

### Ajouter des formules mathématiques

Vous avez la possibilité d’insérer des formules mathématiques écrites en LATEX.
Il faut pour cela les encadrer de ce code HTML :

    <p class="mathjax mathjax--block">…</p>

Exemples :

    <p class="mathjax mathjax--block">
    $$
    \begin{align}
      g_{\mu \nu} & = [S1] \times \operatorname{diag}(-1,+1,+1,+1) \\[6pt]
      {R^\mu}_{\alpha \beta \gamma} & = [S2] \times (\Gamma^\mu_{\alpha \gamma,\beta}-\Gamma^\mu_{\alpha \beta,\gamma}+\Gamma^\mu_{\sigma \beta}\Gamma^\sigma_{\gamma \alpha}-\Gamma^\mu_{\sigma \gamma}\Gamma^\sigma_{\beta \alpha}) \\[6pt]
      G_{\mu \nu} & = [S3] \times {8 \pi G \over c^4} T_{\mu \nu}
    \end{align}
    $$
    </p>

![](images/mathjax1.png)

    <p class="mathjax mathjax--block">
    $$
    R_{\mu \nu}=[S2]\times [S3] \times {R^\alpha}_{\mu\alpha\nu}
    $$
    </p>

![](images/mathjax2.png)

## Page modulaire

Une page modulaire n'est pas une simple page avec tout le contenu au même endroit mais une page qui *fait appel* à des morceaux. Une page ira, par exemple, chercher un morceau "clients" utilisant [le module du même nom](modules.html#clients), puis un autre morceau [Team](modules.html#team) etc…

Pour cela on va devoir&nbsp;:

  * créer une page
  ![image de la création d'une page](images/grav_ajouter_page_modulaire.png)
  * passer en mode **Expert**
  * ajouter dans l'onglet **Contenu**&nbsp;:
    ```
    title: 'Page modulaire'
    metadata:
        description: 'Page modulaire d’exemple.'
    slug: page-modulaire
    content:
        items: '@self.modular'
        order:
            by: default
            dir: asc
            custom:
                - _equipe
    ```
    * **(à modifier)** `title:` est le titre de votre page
    * **(à modifier)** `slug:` son url
    * **(à modifier)** `- _equipe` est le nom du module (voir ci-dessous)
  * enregistrer
  * créer une page modulaire en cliquant sur **Ajouter un contenu modulaire**
  ![image du menu pour ajouter un contenu modulaire](images/grav_ajouter_contenu_modulaire.png)
  * entrer les informations
  ![image d'un ajout de contenu modulaire](images/grav_ajouter_contenu_modulaire_team.png)
    1. Le titre de la page
    * son chemin (créé automatiquement)
    * indiquer sa page parente (dans notre cas `Page modulaire` créée ci-dessus)
    * le *template* (ici `Team`)
  * créer le contenu ([par exemple](modules.html#team))&nbsp;:
    ```
    [g-team attributes="id:_team,class:team module"]

    ## L’équipe
    Utilisez le module `team` pour présenter vos collaborateurs, partenaires, employés, etc…

    ___

    [g-team-item image="jane.jpg" attributes="class:col-md-4"]
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    <div class="item-social">
    [g-link url="#" icon="asterisk" icon_type="fontawesome" stacked=true][/g-link]
    [g-link url="#" icon="retweet" icon_type="fontawesome" stacked=true][/g-link]
    [g-link url="#" icon="envelope" icon_type="fontawesome" stacked=true][/g-link]
    </div>

    [/g-team-item]

    [g-team-item image="mark.jpg" attributes="class:col-md-4"]
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    <div class="item-social">
    [g-link url="#" icon="asterisk" icon_type="fontawesome" stacked=true][/g-link]
    [g-link url="#" icon="retweet" icon_type="fontawesome" stacked=true][/g-link]
    [g-link url="#" icon="envelope" icon_type="fontawesome" stacked=true][/g-link]
    </div>

    [/g-team-item]

    [g-team-item image="julia.jpg" attributes="class:col-md-4"]
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    <div class="item-social">
    [g-link url="#" icon="asterisk" icon_type="fontawesome" stacked=true][/g-link]
    [g-link url="#" icon="retweet" icon_type="fontawesome" stacked=true][/g-link]
    [g-link url="#" icon="envelope" icon_type="fontawesome" stacked=true][/g-link]
    </div>

    [/g-team-item]
    [/g-team]
    ```
  * enregistrer

## Gestion des permissions

### Créer une page privée

Vous avez la possibilité de créer une page qui ne sera visible que par les personnes voulues. Par exemple, pour n'afficher une page qu'à un utilisateur connecté, vous devez :

  * créer l'utilisateur sur https://frama.site/ : https://docs.framasoft.org/fr/framasite/prise-en-main.html#gestion-des-utilisateurs
  * éditer la page que vous souhaitez rendre privée
  * passer l'édition en mode **Expert**
  * ajouter dans Frontmatter :

  ```
  access:
      site:
        login: true
  ```
  * enregistrer la page

Toute personne non connectée verra une demande de connexion à la page. Une personne se connectant pourra voir son contenu.

![Aperçu du mode expert de grav](images/grav_frontmatter_page_prive.png)  

<p class="alert alert-warning">
  Dû à un bug, il n'est pas possible d'ajouter un utilisateur non-admin (voir <a href="https://framagit.org/framasoft/Framasite-/framasite/issues/139" title="Lien vers le ticket Framagit">le ticket en rapport</a>).
</p>

## Administration

### Gestion des utilisateurs

Vous pouvez ajouter des utilisateurs à votre site : soit en tant que **simple** utilisateur avec des droits limités, ou en tant qu'**admin**.

Pour ce faire, [voir la page **Prise en main** de Framasite / Administration](../framasite/prise-en-main.md#gestion-des-utilisateurs).
