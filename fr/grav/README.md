# Framasite

[Framasite](https://frama.site/) est un service libre de création de sites web.

1. Créez votre site
2. Connectez-vous à son interface de gestion
3. Personnalisez et publiez vos pages web

![images framasites](images/site-diaporama-creation02.png)

## Pour aller plus loin&nbsp;:

- Utiliser [Framasite](https://frama.site/)
- [Déframasoftiser Internet](deframasoftiser.html)
- [Prise en main](prise-en-main.html)
- [Composants de base](composants-de-base.html)
- [Composants bootstrap](composants-bootstrap.html)
- [Modules](modules.html)
- [Markdown](markdown.html)
- [Site de démonstration](https://demo-blog.frama.site/)
- Un service proposé dans le cadre de la campagne [contributopia](https://contributopia.org/)
