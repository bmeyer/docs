# Déframasoftiser Framalink / huître

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

Le logiciel [lstu](https://framagit.org/fiat-tux/hat-softwares/lstu) utilisé pour Framalink et huître peut être installé par d'autres ([voir la documentation](https://framacloud.org/fr/cultiver-son-jardin/lstu.html)), comme, par exemple, par un [CHATON](https://chatons.org/fr/find?title=&field_chaton_services_tid=67&field_chaton_type_tid=All).

<div class="alert-info alert">Les fichiers envoyés sont listés dans le navigateur utilisé pour l'envoi. Vous ne pouvez retrouver les fichiers envoyés depuis un autre navigateur. Les informations sont stockées en localStorage : si vous supprimez vos données localStorage, vous perdrez ces informations.</div>

## Exporter

Pour exporter vos données vous devez&nbsp;:

  * cliquer sur le bouton **Mes liens**
  * cliquer sur le bouton **Exporter vos URL**
  * enregistrer le fichier `lstu_export.json` sur votre ordinateur

## Importer

Après avoir téléchargé le fichier `lstu_export.json` sur votre ordinateur, vous pouvez l'importer dans un autre site utilisant le logiciel lstu (voir plus haut).

Pour ce faire, vous devez&nbsp;:

  * cliquer sur le bouton **Mes liens**
  * cliquer sur le bouton **Parcourir…** sous **Importer des URL**
  * récupérer votre fichier `lstu_export.json` sur votre ordinateur téléchargé précédemment (voir ci-dessus)
  * cliquer sur le bouton **Importer des URL**

Les URL seront alors listées sur le nouveau site. Elles restent cependant hébergées sur l'ancien site.
