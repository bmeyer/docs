# Foire Aux Questions

##  Minimiser l'usage du trafic

En cochant cette option à la connexion, vous effectuerez moins de connexions vers le serveur en tâche de fond et vous n'afficherez plus les images.

Ceci peut être utile si vous souhaitez utiliser moins de bande passante (par exemple avec un smartphone) ou si vous voulez accélérer l'affichage des articles (mauvaise qualité de ligne par exemple)

##  Importer ses flux aux format OPML

Dans le menu « Actions », choisir « Configuration », aller sur l’onglet « Flux » puis, en bas, cliquer sur « OPML ».

##  Créer des catégories de flux

Dans le menu « Actions », choisir « Configuration », aller sur l’onglet « Flux » puis cliquer sur « Catégories ».

##  Marquer des articles comme importants

Si vous souhaitez marquer des articles pour une raison ou pour une autre, vous pouvez cliquer sur l’étoile (en haut à gauche de chaque article), ils seront alors accessibles dans le dossier spécial « Articles remarquables ».

##  Étiqueter les articles

Créez des étiquettes dans la configuration (onglet « Étiquettes ») et assignez des étiquettes aux articles avec un clic droit sur le titre de l’article.

Vous aurez alors un dossier « Étiquettes », vous permettant ainsi de créer votre propre classement d’articles.

##  Partager les articles par mail

Cocher l’article (ou les articles) à partager, puis cliquer sur « Plus », « Transférer par email ».

##  Créer son flux personnalisé

Cliquer sur l’icône RSS en haut à gauche de l’article à partager. C’est fait, l’article est dans votre flux personnel

Pour connaître l’adresse de votre flux personnel et pouvoir le partager, dans le menu « Actions », choisir « Configuration », aller sur l’onglet « Flux » puis, en bas, cliquer sur « Articles publiés et partagés / Flux générés ». Vous pouvez aussi aller sur le dossier spécial « Articles publiés » et cliquer sur l’icône RSS en haut à droite de l’écran ou utiliser le menu « Plus », « Voir comme RSS ».

Vous pouvez créer un flux personnalisé pour chacun de vos dossiers et flux en vous positionnant sur le dossier ou le flux et en cliquant sur l’icône RSS en haut à droite ou utiliser le menu « Plus ».

##  Encore plus de partage !

Parmi les plugins activables (voir plus bas), vous trouverez des plugins permettant le partage par Identi.ca, Twitter, Google+, Pinterest…

##  Intégration à Firefox

Pour que Firefox enregistre Framanews comme lecteur de flux, dans le menu « Actions », choisir « Configuration », aller sur l’onglet « Flux » puis, en bas, cliquer sur « Intégration à Firefox ».

Lorsque vous cliquerez sur un lien vers un flux RSS, Firefox vous proposera directement de l’ajouter dans Framanews.

##  Activation de plugins

Ttrss possède un système de plugins, activables dans la configuration.

Dans le menu « Actions », choisir « Configuration », en bas, cliquer sur « Plugins » et faire son choix.

Certains plugins ne sont activables que par les administrateurs, les autres étant à votre discrétion, à l’exception de ceux activés par défaut, que vous ne pourrez pas désactiver.

##  Activation de l'API

Ttrss possède une API permettant l'accès à Ttrss par d'autres clients (voir la partie Mobilité).

Nous n'activons pas cette API par défaut car Ttrss ne nous le permet pas.

Pour activer l'API, rendez-vous dans vos préférences&nbsp;: menu déroulant > **Actions** > **Configuration** > section **Configuration** > section **Général** > **Activer l'accès par l'API**.

##  Utilisation des raccourcis clavier

Ttrss possède un grand nombre de raccourcis claviers très utiles.

Pour consulter la liste des raccourcis claviers disponibles, dans le menu « Actions », choisir « Aide sur les raccourcis clavier ».

Pour les accros à Vim, vous pouvez intervertir les raccourcis clavier des touches `j` et `k` grâce au plugin `swap_jk` et ainsi retrouver un comportement semblable à celui de votre éditeur préféré.
