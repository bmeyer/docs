function u$Lang(lg) {
  var userLang = navigator.languages || [root.navigator.language || root.navigator.userLanguage];
  for (var i = 0; i < userLang.length; i++) {
    if(userLang[i].substring(0,2).toLowerCase() == lg) {
      return true;
    }
  }
}

var lg = window.location.href + ((u$Lang('fr')) ? 'fr' : 'en');
window.location.href = lg
