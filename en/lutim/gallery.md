# Create a gallery on Framapic

<a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a>, (using localstorage), can remember the images you have uploaded to our servers.

All that was missing was a tool to pick from your images to choose what you want to put in your gallery... It's done!

## Proof in pictures

Pouhiou wants to collect the images of the last two Framabooks (<a href="https://framabook.org/la-nef-des-loups/">La Nef des Loups</a>, and <a href="https://framabook.org/grise-bouille-tome-ii/">Grise Bouille Tome 2</a>) and their authors in a gallery. He knows that he has already sent the cover of Grise Bouille and the photo of its author, Gee, to <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a>. So he starts by adding the files from La Nave des loups: the cover of the book and the photo of Yann Kervran, its author.

![Envoi d'images sur Framapic](img/pic-envoi-images.png)

So certainly, Pouhiou sees that he has a link to the gallery of these two images (see the red box), but the other photos are already in <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a>... And then Pouhiou is a bird brain, he forgets to copy this link and closes this tab to go see a knitting video.

A few hours later (yes: knitting is a long process), Pouhiou comes back to <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a> and decides to build his gallery. To do this, he clicks on **My images**.

![mes images sur Framapic](img/pic-mes-images.png)

There, he is in front of a table summarizing the images he has already added to <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a>. All he has to do is check the four that interest him and copy the gallery link at the top (by the way, the little button to the right of the gallery link automatically copies this long link to his clipboard... convenient!)

![ajout d'images dans une galerie Framapic](img/pic-animation-galerie.gif)

Once the link has been copied, all he has to do is paste it into his browser's address bar to see it, or into an email, for example, if he wants to share it. <a href="https://framapic.org/gallery#PaLb7QnHHkDF/HfhhltrLk6UW.png,CQuxwcRFzDiN/c9OHk27wsGEc.png,1NBkcbCgeBO5/0AKZ63dASdf9.png,x5IlRxx45pMu/FoXHKHd2hg4t.jpg">And the result is there</a>!

![résultat d'une galerie Framapic](img/pic-resultat-galerie.png)

Now, here we are, creating an image gallery that respects everyone's data, it's as easy as a few clicks!

## See more
  * Try <a href="https://framapic.org"><b class="violet">Frama</b><b class="vert">pic</b></a> or <a href="https://lut.im">Lut.im</a>
  * Contribute to [Lutim's code](https://framagit.org/luc/lutim)
  * Make donation to Framasky (let's remember that he coded it on his spare time!): [Liberapay](https://liberapay.com/sky/) / [Tipeee](https://tipeee.com/fiat-tux)
