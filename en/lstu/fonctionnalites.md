# Features

## Export/Import link list

Shortcuts created in a browser are saved in **that browser only**: if you use another browser, you will not get a list of your shortcuts.

To find them in another browser you must export them from this one to import them into the new one. To do so:

  * click [My links](https://frama.link/stats) on https://frama.link/ homepage
  * click **Export your URLs**
  * download `lstu_export.json` file on your computer
  * open the other brower (in which you want to find your shortcuts list)
  * open [My links](https://frama.link/stats)
  * click **Browse**
  * select `lstu_export.json` previously downloaded
  * click **Import URLs**
