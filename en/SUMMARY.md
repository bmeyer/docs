# Summary

* [Home](README.md)

## Free services

* [Framaclic / Dolomon](dolomon/README.md)
    * [User case](dolomon/user-case.md)

* [Framadate](framadate/README.md)
    *   [Getting started](framadate/prise-en-main.md)

* [Framadrop / Lufi](lufi/README.md)
    * [Getting started](lufi/prise-en-main.md)

* [Framalink / Lstu](lstu/README.md)
    * [Features](lstu/fonctionnalites.md)

* [Framapiaf / Mastodon](mastodon/README.md)
    * [Getting Started](mastodon/User-guide.md)
    * [Privacy, Safety and Security](mastodon/User-guide.html#privacy-safety-and-security)

* [Framapic / Lutim](lutim/README.md)
    * [Create a gallery](lutim/gallery.md)
    * [How to](lutim/how-to.md)

* [Framasphère / diaspora](diaspora/README.md)
    * [1 - Signing up](diaspora/1-sign_up.md)
    * [2 - Interface](diaspora/2-interface.md)
    * [2b - The interface](diaspora/2b-interface-mobile.md)
    * [3 - Aspects](diaspora/3-aspects.md)
    * [4 - Finding and connecting with people](diaspora/4-connecting.md)
    * [5 - Start sharing!](diaspora/5-sharing.md)
    * [6 - Notifications and conversations](diaspora/6-conversations.md)
    * [7 - Finishing up](diaspora/7-finishing.md)

* [Framateam / Mattermost](mattermost/README.md)

* [Framatalk / Jitsi Meet](jitsimeet/README.md)
    * [How to](jitsimeet/how-to.md)

* [Framavox / Loomio](loomio/README.md)
    * [Getting started](loomio/getting_started.md)
    * [Group settings](loomio/group_settings.md)
    * [Coordinating your group](loomio/coordinating_your_group.md)
    * [Inviting new members](loomio/inviting_new_members.md)
    * [Discussion threads](loomio/discussion_threads.md)
    * [Comments](loomio/comments.md)
    * [Proposals](loomio/proposals.md)
    * [Subgroups](loomio/subgroups.md)
    * [Navigating Loomio](loomio/reading_loomio.md)
    * [Keeping up to date](loomio/keeping_up_to_date.md)
    * [Your user profile](loomio/your_user_profile.md)
