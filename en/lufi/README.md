# Framadrop

[Framadrop](https://framadrop.org) is a free (as in free speech) online service to share files in a confidential way.

  1. If needed you can choose a delay of online availability.
  - Paste the file to share.
  - Then share the given link with your contacts.

Your images are encrypted and hosted on our server and we have no means to decrypt them.

Framadrop is derived from [Lufi](https://lufi.io) software created by [Luc Didry](http://www.fiat-tux.fr/).

## See more

  * [Getting started](prise-en-main.md)
  * [Try Framadrop](https://framadrop.org)
  * [Contribute](https://git.framasoft.org/framasoft/framadrop)
  * [Support us](https://soutenir.framasoft.org/en)
